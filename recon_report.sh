ip=$INV_HUB_IP

GET_ACTIVE_INSTANCE="${ip}/activeInstance"


echo $GET_ACTIVE_INSTANCE
active_instance=$(curl ${GET_ACTIVE_INSTANCE})
if [[ "$active_instance" -eq 1 ]];then
	redis_instance_id="blk-corp-redis1"
else
	redis_instance_id="blk-corp-redis2"
fi

echo $redis_instance_id "is the active instance"
date_now=$(date +'%m%d%Y')
redis_dump_file_name="redis_dump_$date_now.rdb"
gcs_location="gs://allocate-recon-dumps-prod/$redis_dump_file_name"
echo $gcs_location
gcloud redis instances export $gcs_location $redis_instance_id --region=us-east1
#dom_dump_file_name="dom_dump_$date_now.csv"
#dom_dump_gcp_file=$dom_dump_file_name".gz"
#$(gsutil cp gs://allocatable_redis_dump/$redis_dump_file_name .)

#$(gsutil cp gs://allocatable_redis_dump/$dom_dump_gcp_file .)

#if [[ -e $dom_dump_gcp_file ]]; then
#	echo "DOM dump file exists"
#else
#	echo "DOM dump dose not exists, trigger email"
#	exit
#fi

#gzip -f -d $dom_dump_gcp_file

#rdb -c json  --key "(S:*|A:*)" $redis_dump_file_name >> redis_json_dump.txt

#python3 recon_py_script.py $dom_dump_file_name $ip

