#This cell generates csv file
# command to convert RDB to json 
#rdb -c json  --key "(S:*|A:*)" "Memorystore-gcp-corp-redis-dev-Export-2021-06-29 (14_47_19).rdb" >> out1.txt
import pandas as pd
import sys
import json
from datetime import date
import requests
# rdp->json converted file
f = open('redis_json_dump.txt',)
print("#### load json file to memory")
data = json.load(f)
f.close()

print("###Load ignored locations list")
response = requests.get(str(sys.argv[2])+"/getignoreloc")
ignore_locations=response.json()['loc_id']

print("### convert json to csv and store combination of item_location in map data structure")
inv_supply= data[0]
result_set=[]
for item_key in inv_supply:
    fac_supply = inv_supply[item_key]
    for supply in fac_supply:
        if not supply  in ignore_locations:
            result_set.append(fac_supply[supply])
result_dict=[]
today = date.today() 
file_prefix=str(today.month)+str(today.day)+str(today.year)
redis_item_location={}
for item in result_set:
    try:
        d=json.loads(item)
        # print(d)
         
        result_dict.append(d)
        redis_item_location[str(d['item_id'])+"_"+str(d['location_id'])]=d['supply']
        # print(item_location)
        # break
    except Exception as e:
        continue
        #print("cannot convert==>",item,e)
        #  8break
        # 8

# df=pd.DataFrame(result_dict)
# df.to_csv("REDIS_DUMP_"+file_prefix+".csv",index=False)
### Redis dump has been converted to CSV FILE



print("Read DOM inventory picture")
dom_dataframe_item_loc=pd.DataFrame()
dom_dataframe= pd.read_csv(str(sys.argv[1]),converters={'item_id': lambda x: str(x)})
# dom_dataframe['item_location'] = dom_dataframe.agg('{0[item_id]}_{0[location_id]}'.format, axis=1)
print("###Remove ignored locations")
dom_dataframe.drop(dom_dataframe[dom_dataframe.location_id.isin(ignore_locations)].index.tolist())
# indexNames = dom_dataframe[ dom_dataframe['location_id'] in ignore_locations ].index
# # Delete these row indexes from dataFrame
# dom_dataframe.drop(indexNames , inplace=True)

print("###Forming map data structure")
dom_item_location={}
for ind in dom_dataframe.index:
    item_loc_key=str(dom_dataframe['item_id'][ind])+"_"+str(dom_dataframe['location_id'][ind])
    dom_item_location[item_loc_key]=dom_dataframe['supply'][ind]


print("start comparision")
# dom_item_location
missing_in_redis=[]
missing_in_dom=[]
count_variance=[]
dd=dict(dom_item_location)
for key in dd:
    if key in redis_item_location:
        variance = abs( int(dd[key]) - int(redis_item_location[key]))
        if variance >=50 :
            count_variance.append({'item_location':key,'dom_supply':dd[key],'redis_supply':redis_item_location[key]})
    else:
        missing_in_redis.append({'item_location':key})


for key in redis_item_location:
    if not key in dd:
        missing_in_dom.append({'item_location':key})


print("Total items - facility combination in redis: ", len(redis_item_location))
print("Total items - facility combination in DOM: ",len(dom_item_location))


# mfile=open('output-file.txt','w')
# mfile.writelines(missing_in_redis)
# mfile.close()

pd.DataFrame(missing_in_redis).to_csv("MISSING_IN_REDIS.csv",index=False)
pd.DataFrame(missing_in_dom).to_csv("MISSING_IN_DOM.csv",index=False)
pd.DataFrame(count_variance).to_csv("SUPPLY_VARIANCE.csv",index=False)


        


