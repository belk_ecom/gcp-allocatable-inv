#/bin/bash

cd allocatable
echo "generating jar"
gradle build -x test
echo "copy jar from lib to docker folder"
cp build/libs/*.jar docker/allocatable.jar
echo "version =====> " $1
echo "building docker image"
cd docker
docker build -f Dockerfile-prod -t us.gcr.io/inventory-hub-public-prod/allocatable:v$1  .
echo "pushing docker image"
docker push  us.gcr.io/inventory-hub-public-prod/allocatable:v$1

