# This script is to load the ignore location in redis ##

# 1. Connect to the redis get the redis_loc_ignore key
# 2. If it has value download the file and load it into redis
# 3. Delete the file key value in redis. So that this not loaded again.


#RAUTHWORD="pass"
#RHOST="127.0.0.1"
source ~/.bashrc

REDIS_1_password_l=$REDIS_1_password
REDIS_1_host_l=$REDIS_1_host

REDIS_2_host_l=$REDIS_2_host
REDIS_2_password_l=$REDIS_2_password

EXECDIR="/root/allscripts/"
WORKINGDIRECTORY="${EXECDIR}loaddata"
RKEYLOCIGN="loc_ign"
RIGNRKEY="file:ignkey"
ip=$INV_HUB_IP
URLVAL="${ip}/setignoreloc"
ACTIVE_INST_END="${ip}/activeInstance"
BUCKET_NAME=$FULL_LOAD_BUCKET


echo "Load Ignore Start time= $(date)"

# Get the redis value
REDISLOCZFILE=$(redis-cli -h $REDIS_1_host_l -a $REDIS_1_password_l get ${RIGNRKEY})
echo "$(date) :: REDISLOCFILE = ${REDISLOCZFILE}"

#If redis value is empty then return
if [[ -z $REDISLOCZFILE ]]; then
	echo "Redis value is empty"
	exit;
fi

LoadedData=$(curl ${ACTIVE_INST_END})
echo loadedData=$LoadedData

if [ "$LoadedData" -eq 1 ];then
	IN_ACTIVE_P=$REDIS_2_password_l
	IN_ACTIVE_H=$REDIS_2_host_l
	ACTIVE_P=$REDIS_1_password_l
	ACTIVE_H=$REDIS_1_host_l

else
	ACTIVE_P=$REDIS_2_password_l
	ACTIVE_H=$REDIS_2_host_l
	IN_ACTIVE_P=$REDIS_1_password_l
	IN_ACTIVE_H=$REDIS_1_host_l
fi




REDISLOCFILE=$(echo $REDISLOCZFILE | sed 's/\.[^.]*$//' )

#Check if working directory exists if not create it
if [[ ! -d $WORKINGDIRECTORY ]]; then
	DIRCREATED=$(mkdir $WORKINGDIRECTORY)	
	echo "$DIRCREATED== $DIRCREATED"
fi

cd $WORKINGDIRECTORY

# For testing
#::pwd
#::cp ../$REDISLOCFILE .

#Copy the file from the storagebucket
$(gsutil cp gs://$BUCKET_NAME/$REDISLOCZFILE .)

#Unzip the file
$(gzip -dkf "${REDISLOCZFILE}")

if [[ -s $REDISLOCFILE ]]; then
	#Delete the ignore location value
	REDISDEL=$(redis-cli -h $ACTIVE_H -a $ACTIVE_P del ${RKEYLOCIGN})
	echo "REDISPURGE Val ==${REDISPURGE}"


	LOCDET=$(awk -F',' 'FNR > 1 { if ($9 =="N")  {printf " " $1 ""}}' ${REDISLOCFILE})
	echo "LOCDET Val ==${LOCDET}"
	REDISLOCLOAD=$(printf "sadd ${RKEYLOCIGN} ${LOCDET} " | redis-cli -h $ACTIVE_H -a $ACTIVE_P --pipe)
	echo "REDISLOCLOAD Val ==${REDISLOCLOAD}"
else
	echo "File size 0"	
fi

#Deleting the key so that the next run does not pick this up
REDISDELRESPFILE=$(redis-cli -h $ACTIVE_H -a $ACTIVE_P del ${RIGNRKEY})
echo "$(date) :: In Delete REDISDELRESPFILE = ${REDISDELRESPFILE}"

/bin/bash ${EXECDIR}del_ignore_loc.sh "${LOCDET}" "ACTIVE"



#remove the loaded file and zip file from the working directory
$(rm  $REDISLOCFILE)

cd ${EXECDIR}

LoadedData=$(curl ${URLVAL})
echo "From Curl ${LoadedData}"	
echo "Load Ignore End time= $(date)"
exit 0
