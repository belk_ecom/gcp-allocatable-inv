# This checks the size of the file and if it is greater than 10 MB then clear the first 500 lines

LOGFILEPATH=/root/allscripts/logs/loadfulldata.log
FILESIZELIMIT=40000
FILESIZE=$(stat -c %s $LOGFILEPATH)
echo "LOGFILEPATH == $LOGFILEPATH"

if [[ $FILESIZE -gt $FILESIZELIMIT ]]; then
	echo "Filesize is greater than limit "
	sed -i '1,1000d' $LOGFILEPATH 
fi
