source ~/.bashrc
ip=$INV_HUB_IP
ON_ERROR="${ip}/onSyncError"
EXECDIR="/root/allscripts/"
WORKINGDIRECTORY="${EXECDIR}loaddata"
STARTFILE="${EXECDIR}pStarted.txt"

/bin/bash ${EXECDIR}load_full_data.sh
response=$?
if [ "$response" -eq 1 ];then
  cd $WORKINGDIRECTORY
	$(rm -rf *)
	cd ${EXECDIR}
	$(rm $STARTFILE)
	exit 1;
fi
