# This script is to delete the locations that needs to be ignored in the "A:" db of redis. ##
# 1. Get the ignore locations from Redis
# 2. Loop through the locations
# 3. For each location get the corresponding keys and redis command to delete 

#redis-cli -a pass keys "*:311" | awk -F':' '{ print "hdel " $1 " " $2 "\n del " $1":"$2}' | redis-cli --pipe -a pass


#RAUTHWORD="pass"
#RHOST="127.0.0.1"

source ~/.bashrc

REDIS_1_password_l=$REDIS_1_password
REDIS_1_host_l=$REDIS_1_host

REDIS_2_host_l=$REDIS_2_host
REDIS_2_password_l=$REDIS_2_password

EXECDIR="/root/allscripts/"
ip=$INV_HUB_IP
ACTIVE_INST_END="${ip}/activeInstance"
BUCKET_NAME=$FULL_LOAD_BUCKET
LoadedData=$(curl ${ACTIVE_INST_END})
echo loadedData=$LoadedData

if [ "$LoadedData" -eq 1 ];then
	IN_ACTIVE_P=$REDIS_2_password_l
	IN_ACTIVE_H=$REDIS_2_host_l
	ACTIVE_P=$REDIS_1_password_l
	ACTIVE_H=$REDIS_1_host_l

else
	ACTIVE_P=$REDIS_2_password_l
	ACTIVE_H=$REDIS_2_host_l
	IN_ACTIVE_P=$REDIS_1_password_l
	IN_ACTIVE_H=$REDIS_1_host_l
fi

if [ "$2" == "ACTIVE" ];then
  CURR_R_HOST=$ACTIVE_H
  CURR_R_PASS=$ACTIVE_P
else
  CURR_R_HOST=$IN_ACTIVE_H
  CURR_R_PASS=$IN_ACTIVE_P
fi
#If a parameter is passed it will use that for ignoring that location
DIGNORE_LOC=$1

#If empty get the values from the Redis
if [[ -z "$DIGNORE_LOC" ]]; 
then
	UPC_IGNORE_LOC=""

	echo " Del Ignore Start time= $(date)"

	# 1. Reading the redis for ignore locs
	IGNORE_LOC=$(redis-cli -h $CURR_R_HOST -a $CURR_R_PASS smembers "loc_ign")
	#echo " List ${IGNORE_LOC}"

	# 2. Loop through the location and add the values to the variable
	for loc in $IGNORE_LOC; do
		UPC_IGNORE_LOC+="${loc} "
		UPC_IGNORE_LOC_key+="U:key:${loc} "
	done
else
	#if not null set the value
	#echo " In Else ${DIGNORE_LOC}"
	UPC_IGNORE_LOC=$DIGNORE_LOC
fi

#echo "UPC_IGNORE_LOC== ${UPC_IGNORE_LOC}"
#echo "UPC_IGNORE_LOC_key== ${UPC_IGNORE_LOC_key}"

#UPC_IGNORE_LOC_key_SED=$(echo ${UPC_IGNORE_LOC_key} | sed s/key/${replText}/g)
#echo "UPC_IGNORE_LOC_key_SED == ${UPC_IGNORE_LOC_key_SED}"

#Delete the ignore location values in both the Allocatable A:
# 3. For each location get the corresponding keys and redis command to delete
REDISPIPEEXEREP=$(redis-cli -h $CURR_R_HOST -a $CURR_R_PASS keys "A:*" | while read key; do
	printf "hdel ${key} ${UPC_IGNORE_LOC} \n " ;
	done | redis-cli -h $CURR_R_HOST -a $CURR_R_PASS --pipe
	)

#Delete the ignore location values in both the Safetystock bucket S:
# 3. For each location get the corresponding keys and redis command to delete
REDISPIPEEXEREP=$(redis-cli -h $CURR_R_HOST -a $CURR_R_PASS keys "S:*" | while read key; do
	printf "hdel ${key} ${UPC_IGNORE_LOC} \n " ;
	done | redis-cli -h $CURR_R_HOST   -a $CURR_R_PASS --pipe
	)
echo "REDISPIPEEXEREP == ${REDISPIPEEXEREP}"
echo " Del Ignore End time= $(date)"

exit 0
#### Working command for deleting the timestamp too ####
#REDISPIPEEXEREP=$(redis-cli -h $RHOST -a $RAUTHWORD keys "A:*" | while read key; do 
#	printf "hdel ${key} ${UPC_IGNORE_LOC} \n " ; 
#	keyval=$(echo $key | sed s/A://g); 
#	printf "unlink ${UPC_IGNORE_LOC_key} \n" | sed s/key/${keyval}/g ; 
#done | redis-cli -h $RHOST -a $RAUTHWORD --pipe)
