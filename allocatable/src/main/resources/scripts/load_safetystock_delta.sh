# This file loads the Safert stock delta updates to redis
# Perform AWK command on RAW file received from ETL to convert to Redis format
# Load the generated file to redis to perform action on the safety stock delta update
# Need to run as a sudo to have access to gs bucket
source ~/.bashrc

REDIS_1_password_l=$REDIS_1_password
REDIS_1_host_l=$REDIS_1_host

REDIS_2_host_l=$REDIS_2_host
REDIS_2_password_l=$REDIS_2_password


EXECDIR="/root/allscripts/"
STARTFILE="${EXECDIR}sStarted.txt"
WORKINGDIRECTORY="${EXECDIR}loaddata_safety"
ip=$INV_HUB_IP

ACTIVE_INST_END="${ip}/activeInstance"
SAFETY_STOCK_SHA="${ip}/getSafetyStockEvalSha"
STATUS="${ip}/status"
BUCKET_NAME=$FULL_LOAD_BUCKET

if [ -f $STARTFILE ]; then
	echo "Safety stock delta Job running: exiting"
	exit;
fi





# Get the redis value
REDISRESPFILE=$(redis-cli -h $REDIS_1_host_l -a $REDIS_1_password_l get "file:sftkey")
echo "Load Safety stock delta Data $(date) :: REDISRESPFILE = ${REDISRESPFILE}"

echo "File to be processed : $REDISRESPFILE"

#If redis value is empty then return
if [ -z $REDISRESPFILE ]; then
	 ${EXECDIR}file_log_limit.sh
	echo "Redis value is empty"
	exit;
fi

#check status, If the status is Not NO_SYNC_IN_PROGRESS then dont process safety stock delta file.
status=$(curl ${STATUS})
if [ "$status" != "NO_SYNC_IN_PROGRESS" ];then
  echo 'Full sync in process....'
  echo 'exiting'
  exit 0;
fi

LoadedData=$(curl ${ACTIVE_INST_END})
echo loadedData=$LoadedData

if [ "$LoadedData" -eq 1 ];then
	IN_ACTIVE_P=$REDIS_2_password_l
	IN_ACTIVE_H=$REDIS_2_host_l
	ACTIVE_P=$REDIS_1_password_l
	ACTIVE_H=$REDIS_1_host_l

else
	ACTIVE_P=$REDIS_2_password_l
	ACTIVE_H=$REDIS_2_host_l
	IN_ACTIVE_P=$REDIS_1_password_l
	IN_ACTIVE_H=$REDIS_1_host_l
fi




DFILE=$(echo $REDISRESPFILE | sed 's/\.[^.]*$//' )
#:::: echo "var==${REDISRESPFILE}"
#:::: echo "DFILE==${DFILE}"

#Check if working directory exists if not create it
if [ ! -d $WORKINGDIRECTORY ]; then
	DIRCREATED=$(mkdir $WORKINGDIRECTORY)	
	echo "$DIRCREATED== $DIRCREATED"
fi
#Create the start file
touch $STARTFILE


echo "Load safety stock delta file Start ${REDISRESPFILE} -- Process $(date)"

cd $WORKINGDIRECTORY

# For testing
 pwd
ls

#:: REDISRESPFILE="inv1.gz"
#:: cp ../$REDISRESPFILE .

#Copy the file from the storagebucket
$(gsutil cp gs://$BUCKET_NAME/$REDISRESPFILE .)

#Unzip the file
$(gzip -d  "${REDISRESPFILE}")

safetyStockSHA=$(curl ${SAFETY_STOCK_SHA})
REDIS_FILE="redis_sstock.csv"
echo "applying awk command to transform RAW file to Redis command file"
cat $DFILE | awk -v ssSHA="$safetyStockSHA" -F',' 'FNR > 1 {print "evalsha " ssSHA " 1 " $1 " "$2 " " $3 }' > $REDIS_FILE


REDISDELRESPFILE=$(redis-cli -h $REDIS_1_host_l -a $REDIS_1_password_l del "file:sftkey")
echo "$(date) :: In Delete REDISDELRESPFILE = ${REDISDELRESPFILE}"





#Check if the size of the file is greater than zero then load it.

if [ -s $REDIS_FILE ]; then
	

	#Loading safety stock delta data into redis
	REDISLOAD=$(cat ${REDIS_FILE} | redis-cli -h $ACTIVE_H -a $ACTIVE_P --pipe)
	echo "b4REDISLOAD==${REDISLOAD}=="

	REDISLOAD=$(echo -e ${REDISLOAD// '\n'})
	echo "aft=REDISLOAD==${REDISLOAD}=="

	#Set the repsonse of the load value into redis
	REDISPIPEEXELOAD="set 'file:time' '$(date)' \n set 'file:loaded' '${REDISRESPFILE}' \n set 'file:resp' '${REDISLOAD}' \n "
	REDISPIPEEXEREP=$(printf "${REDISPIPEEXELOAD}" | redis-cli -h $ACTIVE_H -a $ACTIVE_P --pipe)
	echo "REDISPIPEEXEREP==${REDISPIPEEXEREP}"

	


else
	echo "File size 0"
	#Deleting the redis key as this file is wrong
  #error=$(curl "${ON_ERROR}?message=In_valid_full_feed_file")
 
 
fi

#remove the loaded file and zip file from the working directory
$(rm  *)
$(rm $STARTFILE )
cd ${EXECDIR} 


echo "Safety stock values are processed ${REDISRESPFILE} -- Process $(date)"




