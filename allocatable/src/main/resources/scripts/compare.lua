-- ARG1 -- facId
-- ARG2 -- epoch
-- ARG3 -- supply
-- ARG4 -- json
-- U bucket stores details of epoch and safetystock
local compareKey="U:" .. KEYS[1] .. ":" .. ARGV[1]

-- get values for key U:upc
local compareValues = redis.call ("hmget", compareKey,"epoch","sstock")



-- return compareValues
local timestamp=-1
local safetystock=-1
 
for i, v in ipairs(compareValues) do if(i==1) then timestamp=v else safetystock=v end end

--compare if current timestamp is greater than existing then update the supply values
if ((timestamp==false) or (tonumber(timestamp) < tonumber(ARGV[2])) ) then
    -- if the upc - location is safetystock enabled load the delta to S bucket else load to A bucket
    if( (safetystock == false) or not(safetystock=="99999" or safetystock=="88888" ))then
    --if the current supply is 0 then delete the value
      if(tonumber(ARGV[3]) <=0) then
             redis.call("hdel","A:"..KEYS[1],ARGV[1])
        else
            redis.call("hset","A:"..KEYS[1],ARGV[1],ARGV[4])
        end
    else      
        if(tonumber(ARGV[3])<=0) then 
            redis.call("hdel","S:"..KEYS[1],ARGV[1]) 
        else
            redis.call("hset","S:"..KEYS[1],ARGV[1],ARGV[4])
        end
    end
    return redis.call("hset",compareKey,"epoch",ARGV[2])
end

return -1;


--  evalsha <SHA> 1 <UPC> <facilityId> <epoch> <SUPPLY> 'JSON'
--- evalsha 323b8eccfefeeb0ecf4fe607bad6dacb09eee48e 1 0438620031201 452 1623396091 33 '{"item_id":0, "location_id":10, "supply":33}'








