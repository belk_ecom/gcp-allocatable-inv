-- KEY  -- Item ID
-- ARG1 -- facId
-- ARG2 -- safetystock

local itemId = KEYS[1];
local facId = ARGV[1];
local sstock = ARGV[2];

local safetystockkey="U:"..itemId..":"..facId;
local sbucketKey = "S:"..itemId;
local abucketKey = "A:"..itemId;
local currentSStock= redis.call("hget",safetystockkey,"sstock");

local function moveTOABucket(newStock)
	--update safetystock value
	redis.call("hset",safetystockkey,"sstock",newStock);
	if (redis.call('hexists',sbucketKey,facId)==1)then
		--get the value
		local value = redis.call("hget", sbucketKey,facId);
		--set the value for new key
		redis.call("hset",abucketKey,facId,value);
		--del old key   
		return redis.call("hdel",sbucketKey,facId);
	end
	return -1;
end

local function moveTOSBucket(newStock)
	--update safetystock value
	redis.call("hset",safetystockkey,"sstock",newStock);
	--get the value
	if (redis.call('hexists',abucketKey,facId)==1)then
		local value = redis.call("hget", abucketKey,facId);
		--set the value for new key
		redis.call("hset",sbucketKey,facId,value);
		--del old key   
		return redis.call("hdel",abucketKey,facId);
	end
	return -1;
end

local function updateStock(newStock)
	--update safetystock value
	return redis.call("hset",safetystockkey,"sstock",newStock);

end

--compare the new safetystock value with exsisting stock value
--1. if curr value is changing 
local function compareAndToggleBuckets(currStock,newStock)
	if ( currStock == '99999' or currStock == '88888')then
		if ( not(newStock=="99999" or newStock=="88888"  ))then
			return moveTOABucket(newStock)
		else	
			return updateStock(newStock)
		end
	else
		if (newStock=="99999" or newStock=="88888"  )then
			return moveTOSBucket(newStock)
		else
			return updateStock(newStock)
		end
	end
	return -1
end

if(currentSStock == false)then
	updateStock(sstock)
	return 0;
else
	return compareAndToggleBuckets(currentSStock,sstock)
end

---evalsha <sha> 1 <UPC> <facilityId> <safetystock value>
--- evalsha 3fa31d1000696a7833c9c5c98b7de7fd12679ce6 1 14000001 2 99999