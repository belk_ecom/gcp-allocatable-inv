# This file loads the full data based on the value on the redis key.
# Since we flush the entire redis cache we also restore few values 
# Like loc_ign and other file loaded details 
# We also call the delete the locations that needs to be ignored on the file

# Need to run as a sudo to have access to gs bucket

source ~/.bashrc

REDIS_1_password_l=$REDIS_1_password
REDIS_1_host_l=$REDIS_1_host

REDIS_2_host_l=$REDIS_2_host
REDIS_2_password_l=$REDIS_2_password
#RAUTHWORD="pass"
#RHOST="127.0.0.1"
EXECDIR="/root/allscripts/"
STARTFILE="${EXECDIR}pStarted.txt"
WORKINGDIRECTORY="${EXECDIR}loaddata"
RKEYLOCIGN="loc_ign"
ip=$INV_HUB_IP
URLVAL="${ip}/fileLoaded"
ACTIVE_INST_END="${ip}/activeInstance"
ON_ERROR="${ip}/onSyncError"
STATUS="${ip}/status"
BUCKET_NAME=$FULL_LOAD_BUCKET

if [[ -e $STARTFILE ]]; then
	echo "Job running: exiting"
	exit;
fi





# Get the redis value
REDISRESPFILE=$(redis-cli -h $REDIS_1_host_l -a $REDIS_1_password_l get "file:key")
echo "Load Data $(date) :: REDISRESPFILE = ${REDISRESPFILE}"



#If redis value is empty then return
if [[ -z $REDISRESPFILE ]]; then
	${EXECDIR}file_log_limit.sh
	echo "Redis value is empty"
	exit;
fi

#check status, If the status is FULL_SYNC_STARTED then proceed with reading the file, else discard the process
status=$(curl ${STATUS})
if [ "$status" != "FULL_SYNC_IN_PROGRESS" ];then
  echo 'Full sync process not initiated'
  echo 'exiting'
  exit 0;
fi

LoadedData=$(curl ${ACTIVE_INST_END})
echo loadedData=$LoadedData


if [ "$LoadedData" -eq 1 ];then
	IN_ACTIVE_P=$REDIS_2_password_l
	IN_ACTIVE_H=$REDIS_2_host_l
	ACTIVE_P=$REDIS_1_password_l
	ACTIVE_H=$REDIS_1_host_l

else
	ACTIVE_P=$REDIS_2_password_l
	ACTIVE_H=$REDIS_2_host_l
	IN_ACTIVE_P=$REDIS_1_password_l
	IN_ACTIVE_H=$REDIS_1_host_l
fi



DFILE=$(echo $REDISRESPFILE | sed 's/\.[^.]*$//' )
#:::: echo "var==${REDISRESPFILE}"
#:::: echo "DFILE==${DFILE}"

#Check if working directory exists if not create it
if [[ ! -d $WORKINGDIRECTORY ]]; then
	DIRCREATED=$(mkdir $WORKINGDIRECTORY)	
	echo "$DIRCREATED== $DIRCREATED"
fi
#Create the start file
touch $STARTFILE

#Get the ignore location as we are going to flush all the keys for loading
# 1. Reading the redis for ignore locs
RIGNORE_LOC=$(redis-cli -h $ACTIVE_H -a $ACTIVE_P smembers "loc_ign")
LIGNORE_LOC=""
# 2. Loop through the location and add the values to the variable
for loc in $RIGNORE_LOC; do
	LIGNORE_LOC+=" ${loc} "
done

echo "Load Full Start ${REDISRESPFILE} -- Process $(date)"

cd $WORKINGDIRECTORY

# For testing
 pwd
ls

#:: REDISRESPFILE="inv1.gz"
#:: cp ../$REDISRESPFILE .

#Copy the file from the storagebucket
$(gsutil cp gs://$BUCKET_NAME/$REDISRESPFILE .)

#Unzip the file
$(unzip  "${REDISRESPFILE}")

REDISDELRESPFILE=$(redis-cli -h $REDIS_1_host_l -a $REDIS_1_password_l del "file:key")
echo "$(date) :: In Delete REDISDELRESPFILE = ${REDISDELRESPFILE}"

#validate checksum of the file
#generate sha256 checksum for the full feed file and compare with the checksum in the checksum file
CFILE="chk_${DFILE}"

if [[ -e $CFILE ]]; then
	echo "checksum file exists in the zip"
else
  echo 'checksum file does not exist in the zip.. exiting'
  curl -X GET "${ON_ERROR}?message=missing_checksum_file"
  exit 1
fi

checksum_in_zip=$(<$CFILE)
echo 'checksum sent in zip:'$checksum_in_zip
cksum=$(sha256sum $DFILE)
checksum=$(echo "$cksum" | sed 's/ .*//')
echo 'calculated checksum:'$checksum
echo 'Checksum=='$checksum}
if [ "$checksum_in_zip" = "$checksum" ];then
        echo "Valid file! checksum is matching"
else
        echo "Invalid file, checksum validation failed"
        echo "Stopping fullfeed process.."
        error=$(curl "${ON_ERROR}?message=checksum_mismatch")
        exit 1
fi




#Check if the size of the file is greater than zero then load it.

if [[ -s $DFILE ]]; then
	REDISPURGE=$(redis-cli -h $IN_ACTIVE_H -a $IN_ACTIVE_P FlushAll)
	echo "REDISPURGE Val ==${REDISPURGE}"

	#Loading data into redis
	REDISLOAD=$(cat ${DFILE} | redis-cli -h $IN_ACTIVE_H -a $IN_ACTIVE_P --pipe)
	echo "b4REDISLOAD==${REDISLOAD}=="

	REDISLOAD=$(echo -e ${REDISLOAD// '\n'})
	echo "aft=REDISLOAD==${REDISLOAD}=="

	#Set the repsonse of the load value into redis
	REDISPIPEEXELOAD="set 'file:time' '$(date)' \n set 'file:loaded' '${REDISRESPFILE}' \n set 'file:resp' '${REDISLOAD}' \n "
	REDISPIPEEXEREP=$(printf "${REDISPIPEEXELOAD}" | redis-cli -h $IN_ACTIVE_H -a $IN_ACTIVE_P --pipe)
	echo "REDISPIPEEXEREP==${REDISPIPEEXEREP}"

	#Load the Ignore location back to redis for future use
	REDISLOCLOAD=$(printf "sadd ${RKEYLOCIGN} ${LIGNORE_LOC} " | redis-cli -h $IN_ACTIVE_H -a $IN_ACTIVE_P --pipe)
	echo "REDISLOCLOAD Val ==${REDISLOCLOAD}"


else
	echo "File size 0"
	#Deleting the redis key as this file is wrong
  error=$(curl "${ON_ERROR}?message=In_valid_full_feed_file")
  exit 1
fi

#remove the loaded file and zip file from the working directory
$(rm  *)

cd ${EXECDIR} 


echo "Load Full End ${REDISRESPFILE} -- Process $(date)"

/bin/bash ${EXECDIR}del_ignore_loc.sh "${LIGNORE_LOC}" "IN_ACTIVE"
response=$?
if [ "$response" -eq 0 ];then
	echo "delete ignore location response:${del_res}"
	LoadedData=$(curl ${URLVAL})
	#Remove the start file
	$(rm $STARTFILE)
	exit 0;
else
  error=$(curl "${ON_ERROR}?message=error_occurred_while_deleting_the_ignored_locations")
	exit 1;
fi
