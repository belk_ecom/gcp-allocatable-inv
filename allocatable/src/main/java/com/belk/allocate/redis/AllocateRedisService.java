/**
 *
 */
package com.belk.allocate.redis;

import com.belk.allocate.CONSTS;
import com.belk.allocate.GCPCloudTaskException;
import com.belk.allocate.InventoryDetailsVO;
import com.belk.allocate.gcp.GCPCloudTaskService;
import com.belk.allocate.gcp.QueueGroupProp;
import com.belk.allocate.hazel.CacheService;
import com.belk.allocate.mail.EmailService;
import com.belk.allocate.req.ItemIdVO;
import com.belk.allocate.req.deltarequest.*;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * This class will have all the redis related activites.
 *
 * @author Subbu Sedu
 *
 */

@Service
public class AllocateRedisService {


    public static final Gson _GSON = new Gson();
    private static final Logger LOGGER = LogManager.getLogger(AllocateRedisService.class);
    public static int DELETED = 2;
    public static String IgnoreLocKey = "loc_ign";
    public static int queCounter = 0;
    public static int backlogQueCounter = 0;

    @Autowired
    EmailService emailService;
    @Autowired
    QueueGroupProp queueGroupProp;
    @Autowired
    RedisUserRepository redisUserRepository;
    @Autowired
    GCPCloudTaskService gcpCloudTaskService;
    @Autowired
    CacheService cacheService;
    @Autowired
    RedisTemplateInstance redisTemplateInstance;


    /**
     * This method loops the item param and gets their respective details
     *
     * @param itemIdVO
     * @return
     */
    public InventoryDetailsVO getAllocatableInventory(ItemIdVO itemIdVO) {

        LOGGER.debug("In AllocateService_Redis getAllocatableInventory ={} ", () -> itemIdVO);

        InventoryDetailsVO inventoryDetailsVO = new InventoryDetailsVO();

        List<String> inventoryDetails = new ArrayList<String>();

        long inCacheTime = System.currentTimeMillis();

        if (itemIdVO != null & itemIdVO.getItems() != null) {

            List<String> redisList = null;
            for (String item : itemIdVO.getItems()) {
                redisList = redisUserRepository.getValues(item);

                inventoryDetails.addAll(redisList);

            }
        }

        inventoryDetailsVO.setInventory(inventoryDetails);

        System.out.println(inventoryDetailsVO.getInventory());

        long outCacheTime = System.currentTimeMillis();

        CONSTS.CACHESTATS_.addCallTime(outCacheTime - inCacheTime);

        return inventoryDetailsVO;
    }

    /**
     * This method sends the item array to the redis pipelined method and gets the value in a single call <br>
     * Then it traverses the Arraylist of Arraylist and converts them into one string Arraylist
     *
     * @param itemIdVO
     * @return
     */
    @SuppressWarnings("unchecked")
    public InventoryDetailsVO getMultiAllocatableInventory(ItemIdVO itemIdVO) {

        LOGGER.debug("In AllocateService_Redis getAllocatableInventory ={} ", () -> itemIdVO);

        InventoryDetailsVO inventoryDetailsVO = new InventoryDetailsVO();

        List<String> inventoryDetails = new ArrayList<String>();

        if (itemIdVO != null & itemIdVO.getItems() != null) {

            long inCacheTime = System.currentTimeMillis();

            List<Object> multiList = redisUserRepository.getMultiValues(itemIdVO.getItems());

            long outCacheTime = System.currentTimeMillis();

            CONSTS.CACHESTATS_.addCallTime(outCacheTime - inCacheTime);

            if (multiList != null && multiList.size() > 0) {

                /*
                 * Move the Arraylist of Arraylist to a Single ArrayList
                 */

                for (Object tObj : multiList) {
                    inventoryDetails.addAll((List<String>) tObj);
                }

                inventoryDetails.replaceAll(s -> s.replace("\\\"", "'"));

                long outCacheTimeAfterArray = System.currentTimeMillis();

                CONSTS.ARRAYLOOPSTATS_.addCallTime(outCacheTimeAfterArray - outCacheTime);

            }

        }

        inventoryDetailsVO.setInventory(inventoryDetails);

        return inventoryDetailsVO;
    }

    /**
     * Based on the UPC and location ID if there is an updated timestamp this method will respond back with that.
     *
     * @param upc
     * @param loc_id
     * @return
     */
    public String getUpdatedTimeStamp(String upc, String loc_id) {
        String updateTimeStamp = "";

        if (upc != null && loc_id != null) {

            String key =  upc + ":" + loc_id;

            updateTimeStamp = redisUserRepository.getValue(CONSTS.UPCLOCFIRSTKEY,key,"epoch");
        }

        return updateTimeStamp;
    }

    /**
     * Based on the UPC and location ID if there is allocatable inventory this will respond back
     *
     * @param upc
     * @param loc_id
     * @return
     */
    public String getAllByUPCLOC(String upc, String loc_id) {
        String updateTimeStamp = "";

        if (upc != null && loc_id != null) {

            updateTimeStamp = redisUserRepository.getValue(upc, loc_id);
        }

        return updateTimeStamp;
    }

    /**
     * Based on the UPC and location ID if there is allocatable inventory value then it will be deleted
     *
     * @param upc
     * @param loc_id
     * @return
     */
    public String deleteByUPCLOC(String upc, String loc_id) {
        String retString = "Record not found";
        String updateTimeStamp = "";

        if (upc != null && loc_id != null) {
            updateTimeStamp = getUpdatedTimeStamp(upc, loc_id);
        }
        if (updateTimeStamp != null) {
            retString = "Record found but not Deleted";

            int iRes = redisUserRepository.deleteUPCByLOC(upc, loc_id);
            if (iRes == DELETED) {
                retString = "Record Deleted";
            }
        }

        return retString;
    }

    /* ************************************************** */
    /* The Delta processing code starts here */
    /* ************************************************** */

    /**
     * This method gets the message body process the message body by passing it to another method. By doing the following things.
     *
     * 1. Puts a try catch around the process and checks for the size of the message. If the message is empty or no data we cannot process it. <br>
     * 2. Checks the size of the delta message and sees if we have any message, then calls the processing method. <br>
     *
     * 3. If the messages are sorted based on the eventdateime the param comes in as true if not comes in false. <br>
     * 4. If the bsorted param comes in as false then we need to process and get the latest message for the upc and field <br>
     * 5. If the batched param comes in as true then we need to process them in batch mode one shot all the messages <br>
     *
     * @param deltaMessage
     * @param bSorted      : True | False
     * @param batched      : True | False
     * @return
     */
    public String processDelta(DeltaMessage deltaMessage, boolean bSorted, boolean batched) {
        String retMessage = "";
        LOGGER.debug("In AllocateService_Redis processDelta ={} ", () -> deltaMessage);
        Map<String, Message> message = null;
        try {
            if (deltaMessage.getDelta().size() > 0) {
                if (bSorted) {
                    message = processSortedMsgArray(deltaMessage.getDelta());
                } else {
                    message = processUnSortedMsgArray(deltaMessage.getDelta());

                }
                if (batched) {
                    DeltaSortMessage deltaSortMessage = new DeltaSortMessage();
                    deltaSortMessage.setMessages(message);
                    processDeltaMessageInBatch(CONSTS.ACTIVE_REDIS_INSTANCE, deltaSortMessage);
                } else {
                    processDeltaMessage(message);
                }

            }
        } catch (NullPointerException npe) {
            LOGGER.error("In AllocateService_Redis processDelta Null pointer exception");
        } catch (Exception e) {
            LOGGER.error("In AllocateService_Redis processDelta exception = {}", () -> e.getStackTrace());
        }

        return retMessage;

    }

    /**
     * This method will do updates in batch mode <br>
     * Will call the compare and load script in redis to perform comparision and push to the appropriate inventory bucket
     *
     *
     * @param deltaSortMessage
     * @return
     */
    public int processDeltaMessageInBatchWithEval(RedisTemplate<String, Object> redisTemplate, DeltaSortMessage deltaSortMessage) {
        int retMsg = -2;
        LOGGER.debug("In AllocateService_Redis processDeltaMessageInBatchWithEval ={} ", () -> deltaSortMessage);
        List<Object> rResp = null;
        Map<String, Message> messages = deltaSortMessage.getMessages();
        if (messages != null && messages.size() > 0) {
            long inTime = System.currentTimeMillis();
            List<ProcessMessage> pMessageList = new ArrayList();
            for (Map.Entry<String, Message> msg : messages.entrySet()) {
                ProcessMessage processMessage = new ProcessMessage(msg.getValue());
                processMessage.setMsgAttributes();
                pMessageList.add(processMessage);
            }
            LOGGER.debug("In processDeltaMessageInBatchWithEval pMessageList={} ", () -> pMessageList);
            if (pMessageList.size() > 0) {
                redisUserRepository.compareAndLoad(redisTemplate, pMessageList);
            }
            long outTime = System.currentTimeMillis();
            long timeTaken = outTime - inTime;
            CONSTS.DELTATASKPROCESS_.addCallTime(timeTaken);
        }

        LOGGER.debug("In AllocateService_Redis processDeltaMessageInBatchWithEval done ");
        return retMsg;
    }


    /**
     * This method will take the unsorted messages and sort them and create tasks for the message sent<br>
     *
     * @param deltaMessage
     * @return
     */
    public String createTaskMessages(DeltaMessage deltaMessage) {
        String retMessage = "";
        LOGGER.debug("In AllocateService_Redis createDeltaMsgTasks ={} ", () -> deltaMessage);
        Map<String, Message> message = null;
        try {
            if (deltaMessage.getDelta().size() > 0) {
                message = processUnSortedMsgArray(deltaMessage.getDelta());
                DeltaSortMessage deltaSortMessage = new DeltaSortMessage();
                deltaSortMessage.setMessages(message);
                createDeltaMsgTask(deltaSortMessage);
                if (CONSTS.STATUS == CONSTS.FULL_SYNC_IN_PROGRESS)
                    createBacklogDeltaMsgTask(deltaSortMessage);
            }
        } catch (NullPointerException npe) {
            LOGGER.error("In AllocateService_Redis createDeltaMsgTasks Null pointer exception");
        }catch (GCPCloudTaskException ge){
            LOGGER.error("In AllocateService_Redis createDeltaMsgTasks exception ", ge);
            throw ge;
        }
        catch (Exception e) {
            LOGGER.error("In AllocateService_Redis createDeltaMsgTasks exception ", e);
        }

        return retMessage;

    }

    /**
     * This method does the following operations returns a final list of objects that needs to be sent to datasource for updating the record. The method assumes
     * the following <br>
     *
     * 1. All the basic checks like null and other java checks are done before this method is called.<br>
     * 2. The list that comes in is sorted based on the updated timestamp and we do not need to sort it again.<br>
     * 3. The last value of the request is the latest value of the record. <br>
     * 4. If the above 3 are not right then the messages may not get updated with the latest data and we need to use a different method.<br>
     *
     * 1. Loops through the messages and creates a hashmap key based on UPC:LOC_ID so that only the latest message is stored <br>
     * 2. Also check if the location needs to be ignored. <br>
     *
     * @param deltas : The messages that needs to be processed
     * @return HashMap of messages with the upc:loc_id key
     */
    public Map<String, Message> processSortedMsgArray(List<Delta> deltas) {
        Map<String, Message> message = new HashMap<String, Message>();

        String key = "";
        for (Delta delta : deltas) {
            Message tmpMsg = delta.getMessage();
            key = tmpMsg.getItem_id() + ":" + tmpMsg.getFacility_id();
            if (isLocationIgnored(tmpMsg.getFacility_id())) {
                continue;
            }
            message.put(key, tmpMsg);
        }

        return message;
    }

    /**
     * Similar to the processSortedMsgArray but this compares the time and updates only the latest one.<br>
     * If the new message is latest then it sets the overwrite message to true and updates the message in Map <br>
     * This also checks if the facility should be ignored.<br>
     *
     * @param deltas
     * @return
     */
    public Map<String, Message> processUnSortedMsgArray(List<Delta> deltas) {
        Map<String, Message> message = new HashMap<String, Message>();

        String key = "";
        boolean overWriteMsg = false;
        for (Delta delta : deltas) {
            overWriteMsg = false;
            Message tmpMsg = delta.getMessage();
            key = tmpMsg.getItem_id() + ":" + tmpMsg.getFacility_id();

            if (isLocationIgnored(tmpMsg.getFacility_id())) {
                continue;
            }

            Message existMsg = message.get(key);
            if (existMsg != null) {
                if (existMsg.getEventDateTime() < tmpMsg.getEventDateTime()) {
                    overWriteMsg = true;
                }

            } else {
                overWriteMsg = true;
            }
            if (overWriteMsg)
                message.put(key, tmpMsg);
        }

        return message;
    }

    /**
     * This method converts the message to Process message and process them individually.
     *
     * @param messages
     * @return
     */
    public int processDeltaMessage(Map<String, Message> messages) {
        int retMsg = -2;
        LOGGER.debug("In AllocateService_Redis processDeltaMessage ={} ");
        for (Map.Entry<String, Message> msgEntry : messages.entrySet()) {
            ProcessMessage processMessage = (ProcessMessage) msgEntry.getValue();
            processMessage.setMsgAttributes();
            int resp = redisUserRepository.compareAndSet(processMessage);
            retMsg = resp;
            LOGGER.debug("Processing Message key={}, responseVal ={} ", () -> msgEntry.getKey(), () -> resp);
        }

        return retMsg;

    }

    /**
     * Not in use
     * This method will do updates in batch mode <br>
     * Will get the values of all the items that needs to be processed and will compare against the timestamp in Redis<br>
     * If the timestamp is less or null then it will update the value in redis<br>
     *
     * @param deltaSortMessage
     * @return
     */
    public int processDeltaMessageInBatch(RedisTemplate<String, Object> redisTemplate, DeltaSortMessage deltaSortMessage) {
        int retMsg = -2;
        LOGGER.debug("In AllocateService_Redis processDeltaMessageInBatch ={} ", () -> deltaSortMessage);
        List<Object> rResp = null;
        Map<String, Message> messages = deltaSortMessage.getMessages();
        if (messages != null && messages.size() > 0) {

            rResp = redisUserRepository.getMultiGet(redisTemplate, CONSTS.UPCLOCFIRSTKEY, messages);

            long inTime = System.currentTimeMillis();

            if (rResp != null) {
                List<ProcessMessage> pMessageList = new ArrayList();
                int iCount = 0;
                Object tmp;
                for (Map.Entry<String, Message> msg : messages.entrySet()) {
                    tmp = rResp.get(iCount++);
                    if (tmp == null || (tmp != null && Long.parseLong((String) tmp) < msg.getValue().getEventDateTime())) {
                        ProcessMessage processMessage = new ProcessMessage(msg.getValue());
                        processMessage.setMsgAttributes();
                        pMessageList.add(processMessage);
                    }
                }
                LOGGER.debug("In processDeltaMessageInBatch pMessageList={} ", () -> pMessageList);
                if (pMessageList.size() > 0) {
                    redisUserRepository.setMultiProcessMsgToDS(redisTemplate, pMessageList);
                }
                long outTime = System.currentTimeMillis();

                long timeTaken = outTime - inTime;

                CONSTS.DELTATASKPROCESS_.addCallTime(timeTaken);

            }

        }

        LOGGER.debug("In AllocateService_Redis processDeltaMessageInBatch done ");
        return retMsg;

    }

    /**
     * Create the task for the messages and load them into the task queue.
     *
     * @param deltaSortMessage
     * @return
     */
    public int createDeltaMsgTask(DeltaSortMessage deltaSortMessage) {
        int retMsg = -2;
        String taskMsg = _GSON.toJson(deltaSortMessage);
        LOGGER.debug("In AllocateService_Redis createDeltaMsgTask ={} ", () -> taskMsg);
        try {
            /**
             * Pushing it to 5 Queues so that queues can grow slowly and data can be processed faster
             */
            int queMod = queCounter++ % queueGroupProp.getDeltaQueue().size();
            if (queCounter > 100)
                queCounter = 0;
            LOGGER.debug("In createDeltaMsgTask queCounter=" + queCounter + "queMod==" + queMod + "Queue Val==" + queueGroupProp.getDeltaQueue().get(queMod));
            // Send create task request.
            gcpCloudTaskService.sendDeltaTask(CONSTS.DELTA_QUEUE_PATH.get(queMod), taskMsg);

            retMsg = 0;
        } catch (Exception e) {
            LOGGER.error("Exception in In AllocateService_Redis createDeltaMsgTask ={} ", () -> e.getMessage());
            throw e;
        }
        return retMsg;
    }

    /**
     * Create the task for the messages and load them into the back log queue.
     *
     * @param deltaSortMessage
     * @return
     */
    public int createBacklogDeltaMsgTask(DeltaSortMessage deltaSortMessage) {
        int retMsg = -2;
        String taskMsg = _GSON.toJson(deltaSortMessage);
        LOGGER.debug("In AllocateService_Redis createBacklogDeltaMsgTask ={} ", () -> taskMsg);
        try {
            /**
             * Pushing it to back log Queues so that queues can grow slowly and data can be processed faster
             */
            int queMod = backlogQueCounter++ % CONSTS.PAUSED_QUEUE_PATHS.size();
            if (backlogQueCounter > 100)
                backlogQueCounter = 0;
            LOGGER.debug("In createBacklogDeltaMsgTask queCounter=" + backlogQueCounter + "queMod==" + queMod + "Queue Val==" + CONSTS.PAUSED_QUEUE_PATHS.get(queMod));
            // Send create task request.
            gcpCloudTaskService.sendBacklogDeltaTask(CONSTS.PAUSED_QUEUE_PATHS.get(queMod), taskMsg);

            retMsg = 0;
        } catch (Exception e) {
            LOGGER.error("Exception in In AllocateService_Redis createBacklogDeltaMsgTask ={} ", () -> e.getMessage());
            throw e;
        }
        return retMsg;
    }


    /**
     * Returns the String of set of location ignore
     *
     * @return
     */
    public LocationIgnore getIgnoreLocation() {
        LocationIgnore logIgn = new LocationIgnore();
        Set<Object> tmpIgLoc = redisUserRepository.getIgnoreLocations(IgnoreLocKey);
        if (tmpIgLoc != null && tmpIgLoc.size() >= 0) {
            logIgn.setLoc_id(new HashSet(tmpIgLoc));
        }

        return logIgn;
    }

    /**
     * Need to check if the passed location should be ignored or allowed. If it needs to be ignored return false.<br>
     * If the static value is null then set the static values.<br>
     *
     */
    public boolean isLocationIgnored(String locations_id) {
        boolean boolReturn = false;
        Set<String> lI = CONSTS.config.getLocationIgnore();
        if (lI == null || lI.size() <= 0) {
            CONSTS.LOCATIONIGNORE = this.getIgnoreLocation();
            lI = CONSTS.LOCATIONIGNORE.getLoc_id();
            CONSTS.config.setLocationIgnore(lI);
        }
        if (lI != null)
            boolReturn = lI.contains(locations_id);

        return boolReturn;
    }

    /**
     * Calculate DB size difference between Inactive REDIS instance and Active REDIS instance
     * @return
     */
    public long getDBSizeDifference() {
        long sizeDiff = 0;
        try {
            sizeDiff = redisUserRepository.getDBSize(CONSTS.ACTIVE_REDIS_INSTANCE) - redisUserRepository.getDBSize(CONSTS.IN_ACTIVE_REDIS_INSTANCE);
            LOGGER.info("DB size difference {}", sizeDiff);
        } catch (Exception e) {
            LOGGER.error("Redis DB check failed! {}", e.getMessage());
            emailService.triggerEmail("Redis DB check failed", "DB size check for redis instance failed", "DB_CHECK_FAILED");
        }
        return sizeDiff;
    }

    /**
     * Method to handle rollback -- to be used if support wants to rollback to previous redis instance
     * If this method is invoked after the timer is resumed roll back will not happen
     * If the redis DB size check fails roll back will not happen
     * If the size difference is with in the required range then rollback to previous instance, else send alert to support team and Redis instance is not switched
     *
     */
    public void rollBackSwitch() {
        LOGGER.info("In rollBackSwitch => roll back to previous redis instance");
        Map<String, Object> configValues = new HashMap<>();
        configValues.put("sendDeltaToBothRedis", 0);
        configValues.put("status", CONSTS.NO_SYNC_IN_PROGRESS);
        if (CONSTS.config.getStatus() != CONSTS.FULL_SYNC_COMPLETED) {
            throw new RuntimeException("Can't roll back timer ran out!");
        }
        if (CONSTS.config.isSizeCheckRequired()) {
            long dbSize = getDBSizeDifference();
            if (CONSTS.config.getMinDiffSize() <= dbSize && dbSize <= CONSTS.config.getMaxDiffSize()) {
                switchRedisInstance(configValues, true);
            } else {

                LOGGER.error("DB size condition failed! active redis size ={} , dormant redis size= {}", redisUserRepository.getDBSize(CONSTS.ACTIVE_REDIS_INSTANCE), redisUserRepository.getDBSize(CONSTS.IN_ACTIVE_REDIS_INSTANCE));
                emailService.triggerEmail("DB size check failed", "DB size difference is not with in the range!", "DB_SIZE_CHECK_FAILED");
                throw new RuntimeException("can't roll back DB size check failed");
            }
        } else {
            LOGGER.debug("DB size check not required! Roll back to old redis instance");
            switchRedisInstance(configValues, true);
        }
    }


    /**
     * Start a timer for a configured time
     * After the timer resumes, check if the status is FULL_SYNC_COMPLETED which indicates no rollback has been done
     * update cache to indicate deltas are only processed to Active redis instance
     */
    public void startTimerBK() {
        LOGGER.info("In AllocateService_Redis startTimer");
        Timer t = new Timer();
        TimerTask tt = new TimerTask() {
            @Override
            public void run() {
                if (CONSTS.config.getStatus() == CONSTS.FULL_SYNC_COMPLETED) {
                    LOGGER.info("In AllocateService_Redis resuming timer");
                    Map<String, Object> configValues = new HashMap<>();
                    configValues.put("sendDeltaToBothRedis", 0);
                    configValues.put("status", CONSTS.NO_SYNC_IN_PROGRESS);

                    cacheService.updateCacheValues(configValues, true);
                    LOGGER.info("Delta messages will only be processed to active redis instance");
                }
            }

        };

        t.schedule(tt, CONSTS.config.getWaitTime());
    }

    /**
     * Toggle between redis instances
     * @param configChanges
     */
    public void switchRedisInstance(Map<String, Object> configChanges, boolean rollback) {
        int currActiveInstance = CONSTS.ACTIVE_INSTANCE;
        Integer activeInstance = currActiveInstance == 1 ? 2 : 1;
        try {
            configChanges.put("activeRedisInstance", activeInstance);
            cacheService.updateCacheValues(configChanges, true);
            LOGGER.debug("Redis instance switched successfully!, current Active Redis instance {}", activeInstance);
            if (!rollback) {
                if (CONSTS.config.isSyncCompleteNotification()) {
                    if (cacheService.get("syncStartAt") != null && CacheService.map.containsKey("syncStartAt")) {
                        long syncTime = System.currentTimeMillis() - (long) cacheService.get("syncStartAt");
                        LOGGER.info("Full sync process took {} seconds", syncTime / 1000);
                        emailService.triggerEmail("Full sync process completed successfully", "Full sync process completed successfully with out any errors in " + (syncTime / 60000) + " minutes", "FULL_SYNC_COMPLETE");
                        CacheService.map.remove("syncStartAt");
                    } else
                        emailService.triggerEmail("Full sync process completed successfully", "Full sync process completed successfully with out any errors", "FULL_SYNC_COMPLETE");
                } else {
                    LOGGER.debug("Skip sending notification on sync completion");

                }

            } else {
                LOGGER.info("Rollbacked to previous instance");
            }
        } catch (Exception e) {
            LOGGER.error("Redis switch failed {}", e.getMessage());
            emailService.triggerEmail("REDIS instance switch failed", "REDIS instance not switched successfully", "REDIS_SWITCH_FAILED");
        }

    }


    public void startTimerForSyncProcess() {
        Timer t = new Timer();
        TimerTask tt = new TimerTask() {
            @Override
            public void run() {

                if (CONSTS.config.getStatus() != CONSTS.NO_SYNC_IN_PROGRESS) {
                    LOGGER.debug("Sync process did not complete with in {} milliseconds", CONSTS.config.getSyncProcessLimit());
                    emailService.triggerEmail("Sync Process taking too long", "Sync process taking too long, please check cloud task queues and logs", "SYNC_PROCESS_RUNNING_TOO_LONG");
                }
            }

        };

        t.schedule(tt, CONSTS.config.getSyncProcessLimit());
    }
}
