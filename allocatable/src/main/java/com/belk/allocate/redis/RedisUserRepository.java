package com.belk.allocate.redis;

import com.belk.allocate.CONSTS;
import com.belk.allocate.req.deltarequest.Message;
import com.belk.allocate.req.deltarequest.ProcessMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.ReturnType;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
public class RedisUserRepository {

    private static final Logger LOGGER = LogManager.getLogger(RedisUserRepository.class);

    public static int UPDATED_ = 0;
    public static int SKIP_ = 1;
    public static int ERROR_ = -1;
    public static int Deleted = 2;
    public SetOperations<String, Object> opsForSets;
    public RedisTemplate<String, Object> redisTemplate;
    HashOperations<String, String, String> hashOperations;
    ValueOperations<String, Object> valOps;


    public RedisUserRepository() {
    }

    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.setRedisOperators();
    }

    public void setRedisOperators() {
        this.hashOperations = this.redisTemplate.opsForHash();
        this.valOps = this.redisTemplate.opsForValue();
        this.opsForSets = this.redisTemplate.opsForSet();
    }

    public String findById(String key, String field) {
        key = CONSTS.ALLOCATABLEFIRSTKEY + key;
        LOGGER.debug("In findById = " + key);

        return hashOperations.get(key, field);

    }

    /**
     * This method gets the multiple fields of the same key.
     *
     * @param key
     * @param fields
     * @return
     */
    public List<String> findManyList(String key, Collection<String> fields) {
        LOGGER.debug("In RedisUserRepository findManyList ");
        key = CONSTS.ALLOCATABLEFIRSTKEY + key;
        return hashOperations.multiGet(key, fields);
    }

    /**
     * Returns the value of the string Key
     *
     * @param key
     * @return
     */
    public String findValById(String key) {
        LOGGER.debug("In findValById ={} ", () -> key);
        return (String) valOps.get(key);
    }

    /**
     * Gets the hvals from the redis hashset.
     *
     * @param key
     * @return
     */
    public List<String> getValues(String key) {
        key = CONSTS.ALLOCATABLEFIRSTKEY + key;
        LOGGER.debug("In getValues = " + key);
        return hashOperations.values(key);
    }

    /**
     * Gets the hvals from the hasset based on the key and field ID.
     *
     * @param key   : of the hashset
     * @param field : field key of the hasset
     * @return
     */
    public String getValue(String prefix,String key, String field) {
        LOGGER.debug("In RedisUserRepository getValue field = {} key = " + key, () -> field);
        key = prefix + key;
        return hashOperations.get(key, field);
    }

    /**
     * Gets the hvals from the hasset based on the key and field ID.
     *
     * @param key   : of the hashset
     * @param field : field key of the hasset
     * @return
     */
    public String getValue(String key, String field) {
        LOGGER.debug("In RedisUserRepository getValue field = {} key = " + key, () -> field);
        key = CONSTS.ALLOCATABLEFIRSTKEY + key;
        return hashOperations.get(key, field);
    }

    /**
     * Method to pass the list of UPCS and get all the values with one connection and not multiple.
     *
     * @param upcs
     * @return
     */
    public List<Object> getMultiValues(List<String> upcs) {
        LOGGER.debug("In RedisUserRepository getMultiValues upcs = {}", () -> upcs);
        List<Object> results = redisTemplate.executePipelined(new org.springframework.data.redis.core.RedisCallback<Object>() {

            @Override
            public Object doInRedis(org.springframework.data.redis.connection.RedisConnection connection) throws org.springframework.dao.DataAccessException {

                for (String upc : upcs) {
                    upc = CONSTS.ALLOCATABLEFIRSTKEY + upc;
                    connection.hVals(upc.getBytes());
                }

                return null;
            }
        });

        return results;

    }

    /**
     * Method to pass the list of UPCS and get all the values with one connection and not multiple.
     *
     * @return
     */
    public List<Object> getMultiGet(RedisTemplate<String, Object> template, String IdKey, Map<String, Message> messages) {
        LOGGER.debug("In RedisUserRepository getMultiGet messages = {}", () -> messages);

        List<Object> results = template.executePipelined(new org.springframework.data.redis.core.RedisCallback<Object>() {

            @Override
            public Object doInRedis(org.springframework.data.redis.connection.RedisConnection connection) throws org.springframework.dao.DataAccessException {

                messages.forEach((key, msg) -> {
                    String keyID = CONSTS.UPCLOCFIRSTKEY + key;
                    connection.get(keyID.getBytes());
                });

                return null;
            }
        });

        return results;

    }

    /**
     * This method will set the JSON object and the epoch timestamp in the datasource.
     *
     * @param processMessage
     * @return
     */
    public int setProcessMsgToDS(ProcessMessage processMessage) {
        LOGGER.debug("In RedisUserRepository setProcessMsgToDS processMessage = {}", () -> processMessage);

        int retMsg = ERROR_;

        List<Object> results = redisTemplate.executePipelined(new org.springframework.data.redis.core.RedisCallback<Object>() {

            @Override
            public Object doInRedis(org.springframework.data.redis.connection.RedisConnection connection) throws org.springframework.dao.DataAccessException {
                connection.set(processMessage.getKey().getBytes(), processMessage.getEpocUpdateTime().getBytes());
                connection.hSet(processMessage.getItemKey().getBytes(), processMessage.getFieldID().getBytes(), processMessage.getAllocatableMsg().getBytes());
                return null;
            }
        });

        if (results != null) {
            retMsg = UPDATED_;
        }

        return retMsg;

    }

    /**
     * This method will set the List of processmessages to the Datasoure along with their allocatable key.<br>
     * And if the quantity is zero then it will delete the field instead of adding the field.
     *
     * @param currRedisTemplate
     * @param processMessages
     * @return
     */
    public int setMultiProcessMsgToDS(RedisTemplate<String, Object> currRedisTemplate, List<ProcessMessage> processMessages) {
        LOGGER.debug("In RedisUserRepository setMultiProcessMsgToDS processMessages = {}", () -> processMessages);
        int retMsg = ERROR_;

        List<Object> results = currRedisTemplate.executePipelined(new org.springframework.data.redis.core.RedisCallback<Object>() {

            @Override
            public Object doInRedis(org.springframework.data.redis.connection.RedisConnection connection) throws org.springframework.dao.DataAccessException {

                processMessages.forEach((processMessage) -> {
                    byte[] bytKey = processMessage.getItemKey().getBytes();
                    byte[] bytField = processMessage.getFieldID().getBytes();

                    if (processMessage.getAllocatable_qty() <= 0) {
                        connection.hDel(bytKey, bytField);
                    } else {
                        connection.hSet(bytKey, bytField, processMessage.getAllocatableMsg().getBytes());

                    }
                    connection.set(processMessage.getKey().getBytes(), processMessage.getEpocUpdateTime().getBytes());
                });

                return null;
            }
        });

        if (results != null) {
            retMsg = UPDATED_;
        }

        return retMsg;

    }

    /**
     * Call the compare and load script with evalsha command
     * If epoch in request is greater than current redis, then move delta to appropriate bucket
     * If safety stock value is 99999/88888 move the value to Safety stock bucket S:UPC, else move to allocation bucket
     * If supply in request is zero then remove the value from redis
     * Update epoch in redis
     * @param currRedisTemplate
     * @param processMessages
     * @return
     */
    public int compareAndLoad(RedisTemplate<String, Object> currRedisTemplate, List<ProcessMessage> processMessages) {
        LOGGER.debug("In RedisUserRepository compareAndLoad processMessages = {}", () -> processMessages);
        int retMsg = ERROR_;

        List<Object> results = currRedisTemplate.executePipelined(new org.springframework.data.redis.core.RedisCallback<Object>() {
            @Override
            public Object doInRedis(org.springframework.data.redis.connection.RedisConnection connection) throws org.springframework.dao.DataAccessException {

                processMessages.forEach((processMessage) -> {
                    byte[] bytKey = processMessage.getItemKey().getBytes();
                    byte[] bytField = processMessage.getFieldID().getBytes();
                    byte[] qty=String.valueOf(processMessage.getAllocatable_qty()).getBytes();

                   connection.evalSha(CONSTS.config.getDeltaUpdateSHA().getBytes(), ReturnType.INTEGER,1,
                           bytKey,bytField,processMessage.getEpocUpdateTime().getBytes(),qty, processMessage.getAllocatableMsg().getBytes());
                });

                return null;
            }
        });

        if (results != null) {
            retMsg = UPDATED_;
        }

        return retMsg;

    }

    public Long getDBSize(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.getConnectionFactory().getConnection().dbSize();
    }

    public String ping(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.getConnectionFactory().getConnection().ping();
    }

    /**
     * This is the method that will compare the value from datasource and if the value is less then it will update the data.
     * <p>
     * 1. Get the last updated epochtime of the upc and facility from the datasource. <br>
     * 1a. If the value is null then directly set the value to the datasource <br>
     * 2. Convert to long value and compare with the current Value and check if it is less than the passed value. <br>
     * 3. If it is less than the passed value then pipe update the json object and the updated timestamp with the new value. <br>
     * 4. If not then skip it.
     *
     * @param processMessage
     * @return
     */
    public int compareAndSet(ProcessMessage processMessage) {
        LOGGER.debug("In RedisUserRepository compareAndSet processMessages = {}", () -> processMessage);
        int retMsg = ERROR_;


        String dsVal = findValById(processMessage.getKey());

        if (dsVal == null) {
            retMsg = setProcessMsgToDS(processMessage);
        } else if (Long.parseLong(dsVal) < processMessage.getEventDateTime()) {
            retMsg = setProcessMsgToDS(processMessage);
        } else {
            retMsg = SKIP_;
        }
        return retMsg;

    }

    /**
     * This method will set the List of processmessages to the Datasoure along with their allocatable key.<br>
     * And if the quantity is zero then it will delete the field instead of adding the field.
     *
     * @param upc
     * @return
     */
    public int deleteUPCByLOC(String upc, String loc_id) {
        LOGGER.debug("In deleteUPCByLOC upc= {}, loc= {}", () -> upc, () -> loc_id);
        int retMsg = ERROR_;
        
        List<Object> results = redisTemplate.executePipelined(new org.springframework.data.redis.core.RedisCallback<Object>() {

            @Override
            public Object doInRedis(org.springframework.data.redis.connection.RedisConnection connection) throws org.springframework.dao.DataAccessException {
                byte[] bytKey = ("A:" + upc).getBytes();
                byte[] bytSKey = ("S:" + upc).getBytes();
                byte[] bytField = loc_id.getBytes();
//                byte[] bytUpdatKey = ("U:" + upc + ":" + loc_id).getBytes();
                connection.hDel(bytKey, bytField);
                connection.hDel(bytSKey, bytField);
//                connection.del(bytUpdatKey);
                return null;
            }
        });

        if (results != null && !results.isEmpty()) {

            try {
                long longRes = (long) results.get(0);
                if (1 == longRes) {
                    retMsg = AllocateRedisService.DELETED;
                } else {
                    retMsg = SKIP_;
                }
            } catch (Exception e) {
                LOGGER.error("In deleteUPCByLOC error= {}", () -> e.getMessage());
            }
        }

        return retMsg;

    }

    /**
     * Gets the set values for the key that is passed as the param.
     *
     * @param key
     * @return
     */
    public Set<Object> getIgnoreLocations(String key) {
        LOGGER.debug("In RedisUserRepository getIgnoreLocations key = {}", () -> key);
        return opsForSets.members(key);
    }

    public String executeEval(String args) {
        // RedisScript

        return "";

    }

}

/*
 * public void onStart(List<BaseRepository> repositoryList) { if (repositoryList == null) return;
 *
 * long startTme = System.currentTimeMillis(); logger.info("x7-repo/x7-id-generator starting.... \n");
 *
 * final String idGeneratorScript = "local hk = KEYS[1] " + "local key = KEYS[2] " + "local id = ARGV[1] " + "local existId = redis.call('hget',hk,key) " +
 * "if tonumber(id) > tonumber(existId) " + "then " + "redis.call('hset',hk,key,tostring(id)) " + "return tonumber(id) "+ "end " + "return tonumber(existId)";
 *
 * RedisScript<Long> redisScript = new DefaultRedisScript<Long>() {
 *
 * @Override public String getSha1(){ return VerifyUtil.toMD5("id_map_key"); }
 *
 * @Override public Class<Long> getResultType() { return Long.class; }
 *
 * @Override public String getScriptAsString() { return idGeneratorScript; } };
 *
 * for (BaseRepository baseRepository : repositoryList) { CriteriaBuilder.ResultMappedBuilder builder = CriteriaBuilder.buildResultMapped(); Class clzz =
 * baseRepository.getClz(); Parsed parsed = Parser.get(clzz); String key = parsed.getKey(X.KEY_ONE); BeanElement be = parsed.getElement(key); if (be.clz ==
 * String.class) continue; builder.reduce(ReduceType.MAX, be.property).paged().ignoreTotalRows(); Criteria.ResultMappedCriteria resultMappedCriteria =
 * builder.get();
 *
 * List<Long> idList = baseRepository.listPlainValue(Long.class,resultMappedCriteria); Long maxId = idList.stream().filter(id -> id !=
 * null).findFirst().orElse(0L); String name = baseRepository.getClz().getName();
 *
 * logger.info("Db    : " + name + ".maxId = " + maxId);
 *
 * List<String> keys = Arrays.asList(IdGeneratorPolicy.ID_MAP_KEY,name); long result = this.stringRedisTemplate.execute(redisScript,keys,String.valueOf(maxId));
 *
 * logger.info("Redis : " + name + ".maxId = " + result);
 *
 * } logger.info(".................................................."); long endTime = System.currentTimeMillis();
 * logger.info("x7-repo/x7-id-generator started, cost time: " + (endTime-startTme) +"ms\n\n"); }
 */