package com.belk.allocate.redis.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "redis.instance1")
public class RedisInstanceOneProp extends RedisProp {
}
