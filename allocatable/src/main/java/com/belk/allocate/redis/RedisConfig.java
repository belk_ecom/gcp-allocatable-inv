package com.belk.allocate.redis;

import com.belk.allocate.redis.prop.RedisPoolProp;
import io.lettuce.core.ClientOptions;
import io.lettuce.core.resource.ClientResources;
import io.lettuce.core.resource.DefaultClientResources;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.stereotype.Component;


@Component

public class RedisConfig {

    @Autowired
    RedisPoolProp poolProp;


    public GenericObjectPoolConfig poolConfig() {
        GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
        genericObjectPoolConfig.setMaxIdle(poolProp.getMaxIdle());
        genericObjectPoolConfig.setMinIdle(poolProp.getMinIdle());
        genericObjectPoolConfig.setMaxTotal(poolProp.getMaxTotal());
        genericObjectPoolConfig.setMaxWaitMillis(poolProp.getMaxWaitMills());
        return genericObjectPoolConfig;
    }

    ClientResources clientResources() {
        return DefaultClientResources.create();
    }

    public ClientOptions clientOptions(){
        return ClientOptions.builder()
                .disconnectedBehavior(ClientOptions.DisconnectedBehavior.REJECT_COMMANDS)
                .autoReconnect(true)
                .build();
    }

    LettucePoolingClientConfiguration clientConfiguration(){
        return LettucePoolingClientConfiguration.builder()
                .poolConfig(poolConfig())
                .clientOptions(clientOptions())
                .clientResources(clientResources())
                .build();
    }


}
