package com.belk.allocate.redis;

import com.belk.allocate.CONSTS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisTemplateInstance {
    @Autowired
    @Qualifier("redisInstanceOneTemplate")
    private RedisTemplate<String, Object> redisTemplateForInstanceOne;

    @Autowired
    @Qualifier("redisInstanceTwoTemplate")
    private RedisTemplate<String, Object> redisTemplateForInstanceTwo;

    public RedisTemplate<String, Object> getRedisTemplate(int instance) {

        switch (instance) {
            case 1:
                return redisTemplateForInstanceOne;

            case 2:
                return redisTemplateForInstanceTwo;

        }
        return null;
    }

    public RedisTemplate<String, Object> getActiveRedisTemplate() {
        int instance = CONSTS.ACTIVE_INSTANCE;
        return getRedisTemplate(instance);
    }


    public RedisTemplate<String, Object> getInActiveRedisTemplate() {
        int instance = CONSTS.ACTIVE_INSTANCE == 1 ? 2 : 1;
        return getRedisTemplate(instance);
    }
}
