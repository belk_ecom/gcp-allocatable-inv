package com.belk.allocate.redis.prop;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "redis.instance2")
public class RedisInstanceTwoProp extends RedisProp {
}