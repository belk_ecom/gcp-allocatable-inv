package com.belk.allocate.redis.prop;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


@Configuration

public class RedisPoolProp {
    @Value("${redis.maxTotal}")
    private int maxTotal;

    @Value("${redis.maxWaitMills}")
    private long maxWaitMills;

    @Value("${redis.MinIdle}")
    private int MinIdle;

    @Value("${redis.MaxIdle}")
    private int MaxIdle;

    @Value("${redis.testWhileIdle}")
    private Boolean testWhileIdle;

    public int getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(int maxTotal) {
        this.maxTotal = maxTotal;
    }

    public long getMaxWaitMills() {
        return maxWaitMills;
    }

    public void setMaxWaitMills(long maxWaitMills) {
        this.maxWaitMills = maxWaitMills;
    }

    public int getMinIdle() {
        return MinIdle;
    }

    public void setMinIdle(int minIdle) {
        MinIdle = minIdle;
    }

    public int getMaxIdle() {
        return MaxIdle;
    }

    public void setMaxIdle(int maxIdle) {
        MaxIdle = maxIdle;
    }

    public Boolean getTestWhileIdle() {
        return testWhileIdle;
    }

    public void setTestWhileIdle(Boolean testWhileIdle) {
        this.testWhileIdle = testWhileIdle;
    }
}