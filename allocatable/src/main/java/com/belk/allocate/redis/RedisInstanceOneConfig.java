package com.belk.allocate.redis;

import com.belk.allocate.redis.prop.RedisInstanceOneProp;
import com.belk.allocate.redis.prop.RedisPoolProp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisInstanceOneConfig {
    private static final Logger LOGGER = LogManager.getLogger(RedisInstanceOneConfig.class);


    @Autowired
    RedisConfig redisConfig;
    @Autowired
    RedisInstanceOneProp instanceOneProp;

    /**
     * create connection factory
     * @return
     */
    @Primary
    @Bean(name = "jedisInstanceOneConnectionFactory")
    LettuceConnectionFactory jedisConnectionFactory() {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration(instanceOneProp.getHost(), instanceOneProp.getPort());
        redisStandaloneConfiguration.setPassword(RedisPassword.of(instanceOneProp.getPassword()));
        return new LettuceConnectionFactory(redisStandaloneConfiguration, redisConfig.clientConfiguration());
    }


    @Bean(name = "redisInstanceOneTemplate")
    public RedisTemplate<?, ?> redisInstanceOneTemplate(@Qualifier("jedisInstanceOneConnectionFactory") LettuceConnectionFactory jf) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        LOGGER.debug("#### Begin RedisConfig redisTemplate host ={} ", () -> jf.getHostName() + ":" + jf.getPort());
        template.setDefaultSerializer(new StringRedisSerializer());
        template.setConnectionFactory(jf);
        return template;
    }


}

/*
 *
 * The following are the code that i tried for serializing the JSON so that it doesn't have a \
 *
 * @Bean public RedisTemplate<String, Object> redisTemplate() { RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
 * redisTemplate.setConnectionFactory(connectionFactory()); redisTemplate.setKeySerializer(new StringRedisSerializer());
 *
 *
 * RedisSerializer<String> redisSerializer = new StringRedisSerializer();
 *
 * Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class); ObjectMapper om = new ObjectMapper();
 * om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY); om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
 * jackson2JsonRedisSerializer.setObjectMapper(om);
 *
 * //template.setConnectionFactory(factory); redisTemplate.setKeySerializer(redisSerializer); redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
 * redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);
 *
 *
 * return redisTemplate; }
 *
 * @Bean public CacheManager redisCacheManager() { RedisSerializationContext.SerializationPair<Object> jsonSerializer =
 * RedisSerializationContext.SerializationPair.fromSerializer(new GenericJackson2JsonRedisSerializer());
 *
 * return RedisCacheManager.RedisCacheManagerBuilder .fromConnectionFactory(redisConnectionFactory) .cacheDefaults( RedisCacheConfiguration.defaultCacheConfig()
 * .entryTtl(Duration.ofDays(1)) .serializeValuesWith(jsonSerializer) ) .build(); }
 *
 *
 * @Bean public RedisTemplate<String, Object> redisTemplate() { RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
 * redisTemplate.setConnectionFactory(jedisConnectionFactory()); redisTemplate.setKeySerializer(new StringRedisSerializer());
 * redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer()); return redisTemplate; }
 */
