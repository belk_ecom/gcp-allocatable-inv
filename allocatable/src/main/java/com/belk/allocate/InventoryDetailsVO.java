/**
 *
 */
package com.belk.allocate;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * This class is the value object for the data that is returned from the cache and we will respond this class as the output to the request.
 *
 * @author Subbu Sedu
 *
 */
public class InventoryDetailsVO implements Serializable {

    private static final long serialVersionUID = 1L;

    // This will hold the Json object from Cache
    @JsonProperty("inventory")
    private List<String> inventory;


    public List<String> getInventory() {
        return inventory;
    }


    public void setInventory(List<String> inventory) {
        this.inventory = inventory;
    }


    @Override
    public String toString() {
        return "{ \"inventory\" :" + inventory + "}";
    }


}
