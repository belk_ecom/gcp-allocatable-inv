package com.belk.allocate;

public class GCPCloudTaskException extends RuntimeException{
    public GCPCloudTaskException(Throwable t){
        super(t);
    }
}
