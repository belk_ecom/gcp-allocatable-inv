package com.belk.allocate.gcp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration

public class AppProp {

    @Value("${gcp.project.id}")
    private String projectId;
    @Value("${gcp.location.id}")
    private String location;
    @Value("$application.url")
    private String applicationUrl;
    @Value("${gcp.queue.depth.threshold}")
    private int depth;
    @Value("${application.waitbeforeswitch}")
    private long waitTimeBeforeSwitch;
    @Value("${application.dbsizecheck.required}")
    private Boolean sizeCheckRequired;
    @Value("${application.dbsizecheck.difflimit}")
    private long diffLimit;

    @Value("gs://${gcp.gcs.allocate-config-file}")
    private Resource gcsFile;


    @Value("${application.mail.from}")
    private String fromMail;


    public String getFromMail() {
        return fromMail;
    }

    public void setFromMail(String fromMail) {
        this.fromMail = fromMail;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getApplicationUrl() {
        return applicationUrl;
    }

    public void setApplicationUrl(String applicationUrl) {
        this.applicationUrl = applicationUrl;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public long getWaitTimeBeforeSwitch() {
        return waitTimeBeforeSwitch;
    }

    public void setWaitTimeBeforeSwitch(long waitTimeBeforeSwitch) {
        this.waitTimeBeforeSwitch = waitTimeBeforeSwitch;
    }

    public Boolean getSizeCheckRequired() {
        return sizeCheckRequired;
    }

    public void setSizeCheckRequired(Boolean sizeCheckRequired) {
        this.sizeCheckRequired = sizeCheckRequired;
    }

    public long getDiffLimit() {
        return diffLimit;
    }

    public void setDiffLimit(long diffLimit) {
        this.diffLimit = diffLimit;
    }


    public Resource getGcsFile() {
        return gcsFile;
    }

    public void setGcsFile(Resource gcsFile) {
        this.gcsFile = gcsFile;
    }
}
