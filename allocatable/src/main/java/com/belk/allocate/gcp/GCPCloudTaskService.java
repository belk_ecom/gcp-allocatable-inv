package com.belk.allocate.gcp;

import com.belk.allocate.CONSTS;
import com.belk.allocate.GCPCloudTaskException;
import com.belk.allocate.mail.EmailService;
import com.google.cloud.tasks.v2.*;
import com.google.cloud.tasks.v2beta3.QueueStats;
import com.google.protobuf.ByteString;
import com.google.protobuf.FieldMask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class interfaces with GCP cloud tasks
 */
@Component
public class GCPCloudTaskService {
    private static final Logger LOGGER = LogManager.getLogger(GCPCloudTaskService.class);


    @Autowired
    AppProp appProp;
    @Autowired
    QueueGroupProp queueGroupProperties;
    @Autowired
    EmailService emailService;


    private CloudTasksClient cloudTaskClient;

    private com.google.cloud.tasks.v2beta3.CloudTasksClient cloudTasksClientV3;

    /**
     * Initialize cloudTaskClient V2 & V3
     */
    GCPCloudTaskService() {
        try {
            cloudTaskClient = CloudTasksClient.create();
            cloudTasksClientV3 = com.google.cloud.tasks.v2beta3.CloudTasksClient.create();
        } catch (IOException e) {
            LOGGER.error("Exception in createCloudTask cloudTaskClient {} ", e.getMessage());
        }

    }


    /**
     * Get depth of a queue
     * pass Read mask as stats to get stats of a queue
     *
     * @param queuePath
     * @return depth of queue, on exception returns -1
     */
    public long geQueueDepth(String queuePath) {
        long depth = -1;
        try {
            FieldMask.Builder fieldBuilder = FieldMask.newBuilder();
            fieldBuilder.addPaths("stats");
            com.google.cloud.tasks.v2beta3.GetQueueRequest queueRequest =
                    com.google.cloud.tasks.v2beta3.GetQueueRequest.newBuilder().setName(queuePath).setReadMask(fieldBuilder).build();
            com.google.cloud.tasks.v2beta3.Queue queue = cloudTasksClientV3.getQueue(queueRequest);
            depth = queue.getStats().getTasksCount();
            LOGGER.debug("In GCPCloudTaskService geQueueDepth  queue depth of {} is =>{} ", queuePath, queue.getStats().getTasksCount());
        } catch (Exception e) {
            LOGGER.error("Error while getting queue depth =>{}", e.getMessage());
        }
        return depth;
    }

    /**
     * Get stats of a queue
     *
     * @param queuePath
     * @return
     */
    public QueueStats getQueueStats(String queuePath) {
        try {
            FieldMask.Builder fieldBuilder = FieldMask.newBuilder();
            fieldBuilder.addPaths("stats");
            com.google.cloud.tasks.v2beta3.GetQueueRequest queueRequest =
                    com.google.cloud.tasks.v2beta3.GetQueueRequest.newBuilder().setName(queuePath).setReadMask(fieldBuilder).build();
            com.google.cloud.tasks.v2beta3.Queue queue = cloudTasksClientV3.getQueue(queueRequest);

            LOGGER.debug("In GCPCloudTaskService geQueueStats  of {} ", queuePath);
            return queue.getStats();
        } catch (Exception e) {
            LOGGER.error("Error while getting queue depth =>{}", e.getMessage());
        }
        return null;
    }

    /**
     * Purge tasks in a queue
     *
     * @param queuePath
     */
    public void purgeQueue(String queuePath) {

        try {
            Queue purgeQueue = cloudTaskClient.purgeQueue(queuePath);
            LOGGER.debug("In GCPCloudTaskService purgeQueue  queue purged ={} ", purgeQueue.getName());
        } catch (Exception e) {
            LOGGER.error("Error while purging Queue", e.getMessage());
        }
    }

    /**
     * Purge tasks in a list of queue
     *
     * @param queuePaths
     */
    public void purgeQueue(List<String> queuePaths) {
        for (String queue : queuePaths) {
            try {
                Queue purgeQueue = cloudTaskClient.purgeQueue(queue);
                LOGGER.debug("In GCPCloudTaskService purgeQueue  queue purged ={} ", purgeQueue.getName());
            } catch (Exception e) {
                LOGGER.error("Error while purging Queue {}", e.getMessage());
            }
        }
    }

    /**
     * Send Endsync tasks to list of queue
     *
     * @param queuePaths
     */
    public void sendEndSyncTasks(List<String> queuePaths) {
        for (String queue : queuePaths) {
            try {
                String url = CONSTS.config.getWriteServiceIp()+queueGroupProperties.getEndSyncUrl() + "?queuePath=" + queue;
                Task task = buildTask(HttpMethod.GET, url, "");
                Task cloudTask = cloudTaskClient.createTask(queue, task);
                LOGGER.debug("In GCPCloudTaskService sendEndSyncTasks  Task Created ={} ", cloudTask.getName());
            } catch (Exception e) {
                LOGGER.error("Error while creating End sync task {}", e.getMessage());
            }
        }
    }

    /**
     * send Endsync tasks to a queue
     *
     * @param queuePath
     */
    public void sendEndSyncTask(String queuePath) {
        String url =CONSTS.config.getWriteServiceIp()+queueGroupProperties.getEndSyncUrl()+ "?queuePath=" + queuePath;
        Task task = buildTask(HttpMethod.GET, url, "");
        try {
            Task cloudTask = cloudTaskClient.createTask(queuePath, task);
            LOGGER.debug("In GCPCloudTaskService sendEndSyncTask  Task Created ={} ", () -> cloudTask.getName());
        } catch (Exception e) {
            LOGGER.error("Error while creating End sync task {}", e.getMessage());
        }


    }

    /**
     * Resume activity of list of queues
     *
     * @param queuePaths
     * @return
     */
    public List<String> resumeQueueActivity(List<String> queuePaths) {
        List<String> resumedQueues = new ArrayList<>();
        for (String queue : queuePaths) {
            try {
                Queue resumeQueue = cloudTaskClient.resumeQueue(queue);
                resumedQueues.add(queue);
                LOGGER.debug("In GCPCloudTaskService resumeQueueActivity  queue resumed ={} ", () -> resumeQueue.getName());
            } catch (Exception e) {
                emailService.triggerEmail("RESUME ACTIVITY FAILED", "Failed to resume activity on queue " + queue, "QUEUE_RESUME_FAILED");
                LOGGER.error("Error while resuming Queue {}", e.getMessage());
            }
        }
        return resumedQueues;
    }

    /**
     * Pause activity on queues
     *
     * @param queuePaths
     * @return
     */
    public List<String> pauseQueueActivity(List<String> queuePaths) {
        List<String> pausedQueue = new ArrayList<>();
        for (String queue : queuePaths) {
            try {
                Queue pauseQueue = cloudTaskClient.pauseQueue(queue);
                pausedQueue.add(queue);
                LOGGER.debug("In GCPCloudTaskService pauseQueueActivity  queue paused ={} ", pauseQueue.getName());
            } catch (Exception e) {
                emailService.triggerEmail("PAUSE ACTIVITY FAILED", "Failed to pause activity on queue " + queue, "QUEUE_PAUSE_FAILED");
                LOGGER.error("Error while pausing Queue {}", e.getMessage());
                throw e;
            }
        }
        return pausedQueue;
    }


    public void sendBacklogDeltaTask(String queuePath, String body) {
        Task task = buildTask(HttpMethod.POST, CONSTS.config.getWriteServiceIp()+queueGroupProperties.getBackLogTaskTrigger(), body);
        try {
            Task cloudTask = cloudTaskClient.createTask(queuePath, task);
            LOGGER.debug("In GCPCloudTaskService sendBacklogDeltaTask  Task Created ={} ", cloudTask.getName());
        } catch (Exception e) {
            LOGGER.error("Error while creating Backlog Delta task {}", e.getMessage());
            throw new GCPCloudTaskException(e);
        }
    }

    public void sendDeltaTask(String queuePath, String body) {
        Task task = buildTask(HttpMethod.POST, CONSTS.config.getWriteServiceIp()+queueGroupProperties.getTaskTrigger(), body);
        try {
            Task cloudTask = cloudTaskClient.createTask(queuePath, task);
            LOGGER.debug("In GCPCloudTaskService sendDeltaTask  Task Created ={} ", cloudTask.getName());
        } catch (Exception e) {
            LOGGER.error("Error while creating Delta task {}", e.getMessage());
            throw new GCPCloudTaskException(e);
        }
    }


    public Task buildTask(HttpMethod method, String targetURL, String body) {
        Task.Builder taskBuilder = Task.newBuilder().setHttpRequest(HttpRequest.newBuilder().putHeaders("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .setBody(ByteString.copyFrom(body, Charset.defaultCharset())).setUrl(targetURL).setHttpMethod(method).build());
        return taskBuilder.build();
    }


    public int getProcessedQueueCount(String parent) {
        int processedQueue = 0;
        try {
            LOGGER.debug("Calculate processed queue count {}", parent);
            FieldMask.Builder fieldBuilder = FieldMask.newBuilder();
            fieldBuilder.addPaths("stats");
            com.google.cloud.tasks.v2beta3.ListQueuesRequest.Builder builder = com.google.cloud.tasks.v2beta3.ListQueuesRequest.
                    newBuilder().setPageSize(1000).setParent(parent).setFilter("name: " + queueGroupProperties.getBacklogQueueSearchPattern()).setReadMask(fieldBuilder);
            com.google.cloud.tasks.v2beta3.CloudTasksClient.ListQueuesPagedResponse response;
            do {
                response = cloudTasksClientV3.listQueues(builder.build());
                List<com.google.cloud.tasks.v2beta3.Queue> queues = response.getPage().getResponse().getQueuesList();
                for (int i = 0; i < queues.size(); i++) {
                    long depth = queues.get(i).getStats().getTasksCount();
                    if (depth == 0 || depth <= appProp.getDepth() + 1) {
                        processedQueue++;
                    }
                }
                builder.setPageToken(response.getNextPageToken());
            } while (response.getNextPageToken() != null && !response.getNextPageToken().equals(""));

            LOGGER.debug("No of processed queues {}", processedQueue);
            return processedQueue;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public Map<String,Long> getCloudTaskStats(String parent){
        LOGGER.debug("Get stats for the cloud tasks");
        Map<String,Long> queueStats=new HashMap<>();
        try{

            FieldMask.Builder fieldBuilder = FieldMask.newBuilder();
            fieldBuilder.addPaths("stats");
            fieldBuilder.addPaths("name");
            com.google.cloud.tasks.v2beta3.ListQueuesRequest.Builder builder = com.google.cloud.tasks.v2beta3.ListQueuesRequest.
                    newBuilder().setPageSize(1000).setParent(parent).setFilter("all").setReadMask(fieldBuilder);
            com.google.cloud.tasks.v2beta3.CloudTasksClient.ListQueuesPagedResponse response;
            do {
                response = cloudTasksClientV3.listQueues(builder.build());
                List<com.google.cloud.tasks.v2beta3.Queue> queues = response.getPage().getResponse().getQueuesList();
                for(com.google.cloud.tasks.v2beta3.Queue queue:queues){
                    queueStats.put(queue.getName(),queue.getStats().getTasksCount());
                }

                builder.setPageToken(response.getNextPageToken());
            } while (response.getNextPageToken() != null && !response.getNextPageToken().equals(""));
        }catch (Exception e){
            LOGGER.error("Error occurred while getting stats!",e);
        }
        return queueStats;
    }
}
