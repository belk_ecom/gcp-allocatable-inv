package com.belk.allocate.gcp;

import com.google.cloud.tasks.v2beta3.LocationName;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("gcp")
public class GCPController {
    @Autowired
    GCPCloudTaskService taskService;
    @Autowired
    AppProp appProp;
    @GetMapping("/queueStatus")
    public ResponseEntity<?> getStatus(){
        return new ResponseEntity<>(taskService.getCloudTaskStats(LocationName.of(appProp.getProjectId(), appProp.getLocation()).toString()), HttpStatus.OK);
    }
}
