package com.belk.allocate.gcp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.WritableResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

@Component
public class GCPStorageService {
    private static final Logger LOGGER = LogManager.getLogger(GCPStorageService.class);

    @Async
    public void writeToStorage(Resource gcpFile, String data) {
        LOGGER.debug("Writing to GCS file {}", data);
        try (OutputStream os = ((WritableResource) gcpFile).getOutputStream()) {
            os.write(data.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String readFromStorage(Resource gcpFile) throws IOException {
        return StreamUtils.copyToString(
                gcpFile.getInputStream(),
                Charset.defaultCharset()) + "\n";
    }
}
