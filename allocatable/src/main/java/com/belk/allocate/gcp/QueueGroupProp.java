package com.belk.allocate.gcp;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.List;


@Configuration
public class QueueGroupProp {
    @Value("${gcp.queue.group.one.triggerendpoint}")
    private String backLogTaskTrigger;
    @Value("#{'${gcp.queue.group.one.queuename}'.split(',')}")
    private List<String> backlogQueue;
    @Value("${gcp.queue.group.one.endsyncurl}")
    private String endSyncUrl;
    @Value("${gcp.queue.group.one.searchpattern}")
    private String backlogQueueSearchPattern;
//    @Value("#{'${application.url}'.concat('${gcp.queue.group.two.triggerendpoint}')}")
    @Value("${gcp.queue.group.two.triggerendpoint}")
    private String taskTrigger;
    @Value("#{'${gcp.queue.group.two.queuename}'.split(',')}")
    private List<String> deltaQueue;

    public String getBacklogQueueSearchPattern() {
        return backlogQueueSearchPattern;
    }

    public void setBacklogQueueSearchPattern(String backlogQueueSearchPattern) {
        this.backlogQueueSearchPattern = backlogQueueSearchPattern;
    }

    public String getBackLogTaskTrigger() {
        return backLogTaskTrigger;
    }

    public void setBackLogTaskTrigger(String backLogTaskTrigger) {
        this.backLogTaskTrigger = backLogTaskTrigger;
    }

    public String getEndSyncUrl() {
        return endSyncUrl;
    }

    public void setEndSyncUrl(String endSyncUrl) {
        this.endSyncUrl = endSyncUrl;
    }

    public List<String> getBacklogQueue() {
        return backlogQueue;
    }

    public void setBacklogQueue(List<String> backlogQueue) {
        this.backlogQueue = backlogQueue;
    }

    public String getTaskTrigger() {
        return taskTrigger;
    }

    public void setTaskTrigger(String taskTrigger) {
        this.taskTrigger = taskTrigger;
    }

    public List<String> getDeltaQueue() {
        return deltaQueue;
    }

    public void setDeltaQueue(List<String> deltaQueue) {
        this.deltaQueue = deltaQueue;
    }
}
