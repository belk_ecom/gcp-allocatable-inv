/**
 *
 */
package com.belk.allocate.req.deltarequest;

import com.google.gson.Gson;

/**
 * This class will be the base class of creating all the POJO's<br>
 *
 * @author Subbu Sedu
 *
 */
public class BasePojo {

    public static Gson _POJOGSON = new Gson();

    public String printJSON(Object obj) {
        return _POJOGSON.toJson(obj);
    }
}
