/**
 *
 */
package com.belk.allocate.req.deltarequest;

import java.util.List;

/**
 * This class will hold the final messages that needs to be processed.
 *
 * @author Subbu Sedu
 *
 */
public class DeltaMessage extends BasePojo {

    private static final long serialVersionUID = 1L;

    private List<Delta> delta;

    public List<Delta> getDelta() {
        return delta;
    }

    public void setDelta(List<Delta> delta) {
        this.delta = delta;
    }

    @Override
    public String toString() {
        return printJSON(this);
    }

}
