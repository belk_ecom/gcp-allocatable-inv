/**
 *
 */
package com.belk.allocate.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 *
 * This class is the value object for the payload that will be sent as a request for this service.
 *
 * @author SEDURSU
 *
 */

public class ItemIdVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("Items")
    private List<String> items;

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "ItemIdVO [items=" + items + "]";
    }

}
