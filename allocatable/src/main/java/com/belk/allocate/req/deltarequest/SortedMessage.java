/**
 *
 */
package com.belk.allocate.req.deltarequest;

import java.util.List;

/**
 * This class will be the root of the delta messages.
 *
 * @author Subbu Sedu
 *
 */
public class SortedMessage extends BasePojo {

    private static final long serialVersionUID = 1L;

    private List<Delta> delta;

    public List<Delta> getDelta() {
        return delta;
    }

    public void setDelta(List<Delta> delta) {
        this.delta = delta;
    }

    @Override
    public String toString() {
        return printJSON(this);
    }
}
