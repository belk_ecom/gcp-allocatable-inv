/**
 *
 */
package com.belk.allocate.req.deltarequest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * This class will hold the final messages that needs to be processed.
 *
 * @author Subbu Sedu
 *
 */
public class DeltaSortMessage extends BasePojo {

    private static final long serialVersionUID = 1L;

    @JsonProperty("messages")
    private Map<String, Message> messages;

    public Map<String, Message> getMessages() {
        return messages;
    }

    public void setMessages(Map<String, Message> messages) {
        this.messages = messages;
    }

    @Override
    public String toString() {
        return printJSON(this);
    }

}
