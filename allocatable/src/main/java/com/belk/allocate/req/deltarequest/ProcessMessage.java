/**
 *
 */
package com.belk.allocate.req.deltarequest;

import com.belk.allocate.CONSTS;

/**
 * This class is used for processing the messages and adds the additional values that are needed for processing this data.
 *
 * @author Subbu Sedu
 *
 */
public class ProcessMessage extends Message {

    private String key;
    private String fieldID;
    private String itemKey;

    private String epocUpdateTime;
    private String allocatableMsg;

    public ProcessMessage() {
        super();
    }

    public ProcessMessage(Message msg) {
        this.item_id = msg.item_id;
        this.allocatable_qty = msg.allocatable_qty;
        this.eventdatetime = msg.eventdatetime;
        this.facility_id = msg.facility_id;
    }

    public void setMsgAttributes() {

        this.key = CONSTS.UPCLOCFIRSTKEY + item_id + ":" + facility_id;
        this.itemKey =  item_id;
        this.fieldID = facility_id;
        this.epocUpdateTime = String.valueOf(eventdatetime);

        allocatableMsg = getDataSourceMessage();
    }

    public String getFieldID() {
        return fieldID;
    }

    public void setFieldID(String fieldID) {
        this.fieldID = fieldID;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getEpocUpdateTime() {
        return epocUpdateTime;
    }

    public void setEpocUpdateTime(String epocUpdateTime) {
        this.epocUpdateTime = epocUpdateTime;
    }

    public String getAllocatableMsg() {
        return allocatableMsg;
    }

    public void setAllocatableMsg(String allocatableMsg) {
        this.allocatableMsg = allocatableMsg;
    }

    /**
     * This method takes cares of forming the message for the datasource in JSON format. <br>
     * Sample MSg : "{\"item_id\": \"0400687392809\", \"location_id\":199, \"supply\":1}" <br>
     *
     * @return String
     */
    public String getDataSourceMessage() {
        StringBuilder sb = new StringBuilder();

        sb.append("{");
        sb.append("\"item_id\": ");
        sb.append("\"");
        sb.append(item_id);
        sb.append("\",");
        sb.append("\"location_id\":");
        sb.append("\"");
        sb.append(facility_id);
        sb.append("\",");
        sb.append("\"supply\":");
        sb.append(allocatable_qty);
        sb.append("}");

        return sb.toString();
    }

    public String getItemKey() {
        return itemKey;
    }

    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    @Override
    public String toString() {
        return printJSON(this);
    }
}
