/**
 *
 */
package com.belk.allocate.req.deltarequest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class will hold the individual properties of the delta messages
 *
 * @author Subbu Sedu
 *
 */
public class Message extends BasePojo {

    private static final long serialVersionUID = 1L;

    protected String item_id;

    protected String facility_id;

    @JsonProperty("eventdatetime")
    protected long eventdatetime;

    protected int allocatable_qty;

    public Message() {
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getFacility_id() {
        return facility_id;
    }

    public void setFacility_id(String facility_id) {
        this.facility_id = facility_id;
    }

    public long getEventDateTime() {
        return eventdatetime;
    }

    public void setEventDateTime(long eventDateTime) {
        this.eventdatetime = eventDateTime;
    }

    public int getAllocatable_qty() {
        return allocatable_qty;
    }

    public void setAllocatable_qty(int allocatable_qty) {
        this.allocatable_qty = allocatable_qty;
    }

    @Override
    public String toString() {
        return printJSON(this);
    }

}
