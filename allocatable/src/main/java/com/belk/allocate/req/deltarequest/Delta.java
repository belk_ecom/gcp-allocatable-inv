package com.belk.allocate.req.deltarequest;

/**
 * This class will hold the body of the Delta message
 *
 * @author Subbu Sedu
 */
public class Delta extends BasePojo {

    private static final long serialVersionUID = 1L;
    private ProcessMessage pMessage;

    public Message getMessage() {
        return pMessage;
    }

    public void setMessage(ProcessMessage pMessage) {
        this.pMessage = pMessage;
    }

    @Override
    public String toString() {
        return printJSON(this);
    }

}