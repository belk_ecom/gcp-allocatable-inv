package com.belk.allocate.req.deltarequest;

import java.util.Date;
import java.util.Set;

/**
 * This class contains the location id that needs to be ignored.
 *
 * @author Subbu Sedu
 */
public class LocationIgnore extends BasePojo {

    private Set<String> loc_id;
    private Date updateDate;

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Set<String> getLoc_id() {
        return loc_id;
    }

    public void setLoc_id(Set<String> loc_id) {
        this.loc_id = loc_id;
        this.updateDate = new Date();
    }

    @Override
    public String toString() {
        return printJSON(this);
    }

}
