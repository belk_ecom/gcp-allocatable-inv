package com.belk.allocate.mail;

import com.belk.allocate.CONSTS;
import com.belk.allocate.gcp.AppProp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

@Service
public class EmailService {
    private static final Logger LOGGER = LogManager.getLogger(EmailService.class);
    @Autowired
    AppProp appProp;
    @Autowired
    private JavaMailSender mailSender;

    @Async
    public void triggerEmail(String subject, String content, String alertType) {
        try {
            LOGGER.debug("Sending email for alert => {}", alertType);
            MimeMessage message = generateMimeMessage(subject, content);
            sendMail(message);
            LOGGER.info("Email sent for alert => {}", alertType);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Email not sent, alert => {}", alertType);
        }

    }


    private MimeMessage generateMimeMessage(String subject, String content) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
        helper.setSubject(subject);
        helper.setText(content);
        helper.setFrom(appProp.getFromMail());
        helper.setTo(CONSTS.config.getAlertTo().toArray(new String[0]));

        return message;

    }

    private void sendMail(MimeMessage message) {

        mailSender.send(message);

    }
}
