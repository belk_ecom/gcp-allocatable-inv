/**
 *
 */
package com.belk.allocate;

import com.belk.allocate.hazel.CacheService;
import com.belk.allocate.mail.EmailService;
import com.belk.allocate.redis.AllocateRedisService;
import com.belk.allocate.redis.RedisUserRepository;
import com.belk.allocate.req.ItemIdVO;
import com.belk.allocate.req.deltarequest.DeltaMessage;
import com.belk.allocate.req.deltarequest.DeltaSortMessage;
import com.belk.allocate.req.deltarequest.LocationIgnore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static com.belk.allocate.req.deltarequest.BasePojo._POJOGSON;

/**
 * This class will be the controller for this service.
 *
 * @author SEDURSU
 *
 */
@RestController
public class AllocateController {

    private static final Logger LOGGER = LogManager.getLogger(AllocateController.class);
    @Autowired
    CacheService cacheService;

    @Autowired
    EmailService emailService;
    @Autowired
    AllocateRedisService allocateService_Redis;
    @Autowired
    RedisUserRepository redisUserRepository;
    @Autowired
    @Qualifier("redisInstanceOneTemplate")
    private RedisTemplate<String, Object> redisTemplateForInstanceOne;

    @Autowired
    @Qualifier("redisInstanceTwoTemplate")
    private RedisTemplate<String, Object> redisTemplateForInstanceTwo;


    @GetMapping(path = "getSafetyStockEvalSha")
    public String getSafetyStockEvalSha(){
        return CONSTS.config.getSafetyStockSHA();
    }

    @GetMapping(path = "activeInstance")
    public long getActiveInstance() {
        try {
            LOGGER.debug("#### Getting current active redis instance");
            return CONSTS.ACTIVE_INSTANCE;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }

    @GetMapping("healthCheck")
    public String healthCheck(@RequestParam int instance) {
        if (instance == 1) {
            return redisUserRepository.ping(redisTemplateForInstanceOne);
        } else {
            return redisUserRepository.ping(redisTemplateForInstanceTwo);

        }
    }

    @GetMapping("dbSize")
    public long getDBSize(@RequestParam int instance) {
        if (instance == 1) {
            return redisUserRepository.getDBSize(redisTemplateForInstanceOne);
        } else {
            return redisUserRepository.getDBSize(redisTemplateForInstanceTwo);
        }
    }

    @GetMapping(path = "/ping")
    public String pong() {
        LOGGER.debug("#### Begin AllocateController pong");
        return "pong";
    }


    @GetMapping("dbSizeDiff")
    public long getRedisDBSizeDiff() {
        return allocateService_Redis.getDBSizeDifference();
    }

    /**
     * This method is the controller for receiving the objects and send it to the service method and the value from the Cache and respond back.
     *
     * @param itemIDVO
     * @return
     */
    @PostMapping(path = "/mallocatableinv", produces = "application/json;")
    public String getMultiAllocatableInventory(@RequestBody ItemIdVO itemIDVO) {
        LOGGER.debug("#### Begin AllocateController getMultiAllocatableInventory ItemIdVO={} ", () -> itemIDVO);

        long inTime = System.currentTimeMillis();
        //TODO
        InventoryDetailsVO inventoryDetailsVO = allocateService_Redis.getMultiAllocatableInventory(itemIDVO);
        String retStr = returnToString(inventoryDetailsVO);

        long outTime = System.currentTimeMillis();

        long timeTaken = outTime - inTime;

        CONSTS.SERVICEMULTISTATS_.addCallTime(timeTaken);

        LOGGER.info("##$$ CS= {} ", () -> CONSTS.CACHESTATS_);
        LOGGER.info("##$$ ALL= {} ", () -> CONSTS.ARRAYLOOPSTATS_);
        LOGGER.info("##$$ SMSF= {} ", () -> CONSTS.SERVICEMULTISTATS_);
        LOGGER.debug("#### End AllocateController getMultiAllocatableInventory ");

        return retStr;
    }

    /**
     * This controller method passes the value to service to get the time stamp of the update.
     *
     * @param upc
     * @param loc_id
     * @return
     */
    @GetMapping(path = "/up_ts/{upc}/{loc_id}")
    public String getUpdateTimeStamp(@PathVariable String upc, @PathVariable String loc_id) {
        String retStr = "";
        retStr = allocateService_Redis.getUpdatedTimeStamp(upc, loc_id);
        return retStr;

    }

    /**
     * This controller method passes the value to service to get the allocatable inventory by upc and location id.
     *
     * @param upc
     * @param loc_id
     * @return
     */
    @GetMapping(path = "/allocatble_upc_loc/{upc}/{loc_id}", produces = "application/json")
    public String allocatableByUPCLOC(@PathVariable String upc, @PathVariable String loc_id) {
        String retStr = "";
        retStr = allocateService_Redis.getAllByUPCLOC(upc, loc_id);
        return retStr;

    }



    /**
     * This method is the controller to delete a record for a UPC location
     *
     * @param upc
     * @param loc_id
     * @return
     */
    @GetMapping(path = "/del_upc_loc/{upc}/{loc_id}")
    public String delRecordByUPCLoc(@PathVariable String upc, @PathVariable String loc_id) {
        String retString = "Delete Failed";

        retString = allocateService_Redis.deleteByUPCLOC(upc, loc_id);
        return retString;
    }


    /**
     * This method checks if the object is not null and returns the toString of the Inv Details object. If null it returns an empty String.
     *
     * @param inventoryDetailsVO
     * @return
     */
    public String returnToString(InventoryDetailsVO inventoryDetailsVO) {
        String retStr = "";
        if (inventoryDetailsVO != null) {

            retStr = inventoryDetailsVO.toString();
        }
        return retStr;
    }

    /* ************************************************** */
    /* The Delta processing code starts here */
    /* ************************************************** */


    /**
     * This method will get the unsorted delta message and send it to the service layer to create the tasks
     *
     * @param deltaMessage
     * @return
     */
    @PostMapping(path = "/upddeltamsgt")
    public String updateDeltaMsgTasks(@RequestBody DeltaMessage deltaMessage) {
        String retMessage = "";
        LOGGER.debug("#### Begin AllocateController updateDeltaMsgTasks  deltaMessage={} ", () -> deltaMessage);

        long inTime = System.currentTimeMillis();

        allocateService_Redis.createTaskMessages(deltaMessage);

        long outTime = System.currentTimeMillis();

        long timeTaken = outTime - inTime;

        CONSTS.DELTASTATSTASK_.addCallTime(timeTaken);

        LOGGER.debug("##$$ CS= {} ", () -> CONSTS.DELTASTATSTASK_);
        LOGGER.debug("#### End AllocateController updateDeltaMsgTasks ");

        return retMessage;
    }

    /**
     * Get the message and pass it to the data source function to process the messages. This is called from tasks
     *
     * @param deltaSortMessage
     * @return
     */
    @PostMapping(path = "/proctaskmsg")
    public ResponseEntity<String> processTaskMessage(@RequestHeader Map<String, String> headers, @RequestBody DeltaSortMessage deltaSortMessage) {
        try {
            String retMessage = "";
            LOGGER.debug("#### Begin AllocateController processTaskMessage  processTaskMessage={} ", () -> deltaSortMessage);

            long inTime = System.currentTimeMillis();


            if (CONSTS.SEND_DELTA_TO_BOTH_REDIS == 1) {
                allocateService_Redis.processDeltaMessageInBatchWithEval(CONSTS.IN_ACTIVE_REDIS_INSTANCE, deltaSortMessage);
                LOGGER.debug("#### Post message to Inactive Redis instance");

            }

            allocateService_Redis.processDeltaMessageInBatchWithEval(CONSTS.ACTIVE_REDIS_INSTANCE, deltaSortMessage);

            long outTime = System.currentTimeMillis();

            long timeTaken = outTime - inTime;

            CONSTS.DELTASTATS_.addCallTime(timeTaken);

            LOGGER.debug("##$$ DLL= {} ", () -> CONSTS.DELTASTATS_);
            LOGGER.debug("##$$ DTP= {} ", () -> CONSTS.DELTATASKPROCESS_);

            LOGGER.debug("#### End AllocateController updateUnSortedDeltaAllInv ");

            return new ResponseEntity<>(retMessage, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return handleLastRetry(headers, deltaSortMessage, e.getMessage());
        }
    }

    private ResponseEntity<String> handleLastRetry(Map<String, String> headers, DeltaSortMessage message, String errorMessage) {
        int currRetryCount = Integer.parseInt(headers.getOrDefault("x-cloudtasks-taskretrycount", "0"));
        if (currRetryCount == CONSTS.config.getMaxTaskRetryCount() - 1) {
            String taskName = headers.getOrDefault("x-cloudtasks-taskname", "");
            LOGGER.error("Cloud task {} has reached max retry limit and the task will be deleted from queue", taskName);
            String body = String.format("Cloud task %s have failed task => %s", taskName, _POJOGSON.toJson(message));
            emailService.triggerEmail("TASK failure exceeded retry limit", body, "TASK_RETRY_LIMIT_EXCEEDED");
        }
        return new ResponseEntity<>("Failed due to " + errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    /**
     * Get the backlog message and pass it to the data source function to process the messages,
     * Eventually this message will be stored in inactive Redis instance <br>
     * This is called from Backlog task queue
     *
     * @param deltaSortMessage
     * @return
     */
    @PostMapping(path = "/processBacklogTaskmsg")
    public ResponseEntity<String> processBackLogTaskMessage(@RequestHeader Map<String, String> headers, @RequestBody DeltaSortMessage deltaSortMessage) {
        try {

            String retMessage = "";
            LOGGER.debug("#### Begin AllocateController processTaskMessage  processTaskMessage={} ", () -> deltaSortMessage);

            long inTime = System.currentTimeMillis();

            allocateService_Redis.processDeltaMessageInBatchWithEval(CONSTS.IN_ACTIVE_REDIS_INSTANCE, deltaSortMessage);

            long outTime = System.currentTimeMillis();

            long timeTaken = outTime - inTime;

            CONSTS.DELTASTATS_.addCallTime(timeTaken);

            LOGGER.debug("##$$ DLL= {} ", () -> CONSTS.DELTASTATS_);
            LOGGER.debug("##$$ DTP= {} ", () -> CONSTS.DELTATASKPROCESS_);

            LOGGER.debug("#### End AllocateController updateUnSortedDeltaAllInv ");

            return new ResponseEntity<>(retMessage, HttpStatus.OK);
        } catch (Exception e) {
            return handleLastRetry(headers, deltaSortMessage, e.getMessage());
        }
    }


    /**
     * Get the list of ignore locations from the static variable if it is not null. If it is null set the new value in the static variable.
     *
     * @returns : The String of locations that needs to be ignored.
     */
    @GetMapping(path = "/getignoreloc")
    public String getIgnoreLocations() {
        LocationIgnore lI = allocateService_Redis.getIgnoreLocation();
        if (CONSTS.LOCATIONIGNORE == null || CONSTS.LOCATIONIGNORE.getLoc_id() != null) {
            CONSTS.LOCATIONIGNORE = allocateService_Redis.getIgnoreLocation();
            lI = CONSTS.LOCATIONIGNORE;
        }

        return lI.toString();
    }

    /**
     * Get the list of ignore locations and set it in the static value for future use.
     *
     * @returns : The String of locations that needs to be ignored.
     */
    @GetMapping(path = "/setignoreloc")
    public String setIgnoreLocations() {
        CONSTS.LOCATIONIGNORE = allocateService_Redis.getIgnoreLocation();
        Map<String, Object> map = new HashMap<>();
        map.put("locationIgnore", CONSTS.LOCATIONIGNORE.getLoc_id());
        cacheService.updateCacheValues(map, false);
        return _POJOGSON.toJson(CONSTS.LOCATIONIGNORE);
    }

    /* ************************************************** */
    /* Not Used */
    /* ************************************************** */

    /**
     * This method will get the unsorted delta message based on the event date time and pass it to the service layer.
     *
     * @param deltaMessage
     * @return
     */
    @PostMapping(path = "/deltaunsortinvup")
    public String updateUnSortedDeltaAllInv(@RequestBody DeltaMessage deltaMessage) {
        String retMessage = "";
        LOGGER.debug("#### Begin AllocateController updateUnSortedDeltaAllInv  deltaMessage={} ", () -> deltaMessage);

        long inTime = System.currentTimeMillis();

        allocateService_Redis.processDelta(deltaMessage, false, false);

        long outTime = System.currentTimeMillis();

        long timeTaken = outTime - inTime;

        CONSTS.DELTASTATS_.addCallTime(timeTaken);

        LOGGER.info("##$$ CS= {} ", () -> CONSTS.DELTASTATS_);
        LOGGER.debug("#### End AllocateController updateUnSortedDeltaAllInv ");

        return retMessage;
    }

    /**
     * This method will get the unsorted delta message based on the event date time and pass it to the service layer.
     * This is for batching the request.
     *
     * @param deltaMessage
     * @return
     */
    @PostMapping(path = "/deltaupbatch")
    public String deltaUpdateBatch(@RequestBody DeltaMessage deltaMessage) {
        String retMessage = "";
        LOGGER.debug("#### Begin AllocateController updateUnSortedDeltaAllInv  deltaMessage={} ", () -> deltaMessage);

        long inTime = System.currentTimeMillis();

        allocateService_Redis.processDelta(deltaMessage, false, true);

        long outTime = System.currentTimeMillis();

        long timeTaken = outTime - inTime;

        CONSTS.DELTASTATS_.addCallTime(timeTaken);

        LOGGER.info("##$$ CS= {} ", () -> CONSTS.DELTASTATS_);
        LOGGER.debug("#### End AllocateController updateUnSortedDeltaAllInv ");

        return retMessage;
    }

    /**
     * This method is the controller for receiving the objects and send it to the service method and the value from the Cache and respond back.
     *
     * @param itemIDVO
     * @return
     */
    @PostMapping(path = "/allocatableinv", produces = "application/json;")
    public String getAllocatableInventory(@RequestBody ItemIdVO itemIDVO) {
        LOGGER.debug("#### Begin AllocateController getAllocatableInventory ItemIdVO={} ", () -> itemIDVO);

        long inTime = System.currentTimeMillis();

        InventoryDetailsVO inventoryDetailsVO = allocateService_Redis.getAllocatableInventory(itemIDVO);

        String retStr = returnToString(inventoryDetailsVO);

        long outTime = System.currentTimeMillis();

        long timeTaken = outTime - inTime;

        CONSTS.SERVICEMULTISTATSFOR_.addCallTime(timeTaken);

        LOGGER.info("##$$ CS= {} ", () -> CONSTS.CACHESTATS_);
        LOGGER.info("##$$ SMS= {} ", () -> CONSTS.SERVICEMULTISTATSFOR_);

        LOGGER.debug("#### End AllocateController getAllocatableInventory ");
        return retStr;
    }

    /**
     * This method will get the sorted delta message based on the event date time and pass it to the service layer.
     *
     * @param deltaMessage
     * @return
     */
    @PostMapping(path = "/deltasortinvup")
    public String updateSortedDeltaAllInv(@RequestBody DeltaMessage deltaMessage) {
        String retMessage = "";
        LOGGER.debug("#### Begin AllocateController updateSortedDeltaAllInv  deltaMessage={} ", () -> deltaMessage);

        long inTime = System.currentTimeMillis();

        retMessage = allocateService_Redis.processDelta(deltaMessage, true, false);

        long outTime = System.currentTimeMillis();

        long timeTaken = outTime - inTime;

        CONSTS.DELTASTATS_.addCallTime(timeTaken);

        return retMessage;
    }


}
