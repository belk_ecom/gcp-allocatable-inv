package com.belk.allocate;

import com.belk.allocate.gcp.AppProp;
import com.belk.allocate.gcp.GCPCloudTaskService;
import com.belk.allocate.hazel.CacheService;
import com.belk.allocate.mail.EmailService;
import com.belk.allocate.redis.AllocateRedisService;
import com.google.cloud.tasks.v2beta3.LocationName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class FullSyncController {
    private static final Logger LOGGER = LogManager.getLogger(FullSyncController.class);

    @Autowired
    AppProp appProp;
    @Autowired
    GCPCloudTaskService gcpCloudTaskService;
    @Autowired
    CacheService cacheService;
    @Autowired
    EmailService emailService;
    @Autowired
    AllocateRedisService redisService;

    @GetMapping("testEmail")
    public void testEmail(){
            emailService.triggerEmail("TEST","TEST EMAIL","TEST");
    }
    /**
     * This controller is for receiving a full sync start message when ETL starts full sync job
     * set status to FULL_SYNC_IN_PROGRESS
     * Initiate the process if there is no sync in progress
     */
    @GetMapping("/startSync")
    public ResponseEntity<String> startSync() throws IOException {
        if (CONSTS.config.getStatus() == CONSTS.NO_SYNC_IN_PROGRESS) {
            LOGGER.info("#### Begin full sync process");
            Map<String, Object> configValues = new HashMap<>();
            configValues.put("sendDeltaToBothRedis", 0);
            configValues.put("processedQueueCount", 0);
            configValues.put("status", CONSTS.FULL_SYNC_IN_PROGRESS);
            configValues.put("resumedQueue", new ArrayList<>());
            try {
                List<String> pausedQueues = gcpCloudTaskService.pauseQueueActivity(CONSTS.BACKLOG_QUEUE_PATH);
                configValues.put("pausedQueue", pausedQueues);
            } catch (Exception e) {
                emailService.triggerEmail("Start sync failed", "Failure while starting sync process " + e.getMessage(), "START_SYNC_FAILURE");
                return new ResponseEntity<>("Start sync process failed while pausing queues", HttpStatus.BAD_REQUEST);
            }
            configValues.put("syncStartAt", System.currentTimeMillis());
            cacheService.updateCacheValues(configValues, true);
            LOGGER.info("#### Full sync started successfully");
            return new ResponseEntity<>("Sync process started successfully", HttpStatus.OK);
        } else {
            emailService.triggerEmail("Sync already in-progress","Sync is already in progress, Check cloud task queue status, Reset status to start a new sync","SYNC_IN_PROGRESS");
            return new ResponseEntity<>("Sync already in In-progress", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/status")
    public String getStatus() {
        LOGGER.debug("#### Getting status of the sync progress");
        int status = CONSTS.config.getStatus();
        String res = "";
        switch (status) {
            case -1: {
                res = "NO_SYNC_IN_PROGRESS";
                break;
            }
            case 1: {
                res = "FULL_SYNC_IN_PROGRESS";
                break;
            }
            case 2: {
                res = "FULL_SYNC_LOADED";
                break;
            }
            case 3: {
                res = "FULL_SYNC_COMPLETED";
                break;
            }
        }
        return res;
    }

    /**
     * This controller is called when error occurs while loading full sync file
     * Set status to NO_SYNC_IN_PROGRESS
     * Purge all the messages in backlog queue
     * Send alert to Support team
     */
    @GetMapping("onSyncError")
    public void handleErrorOnSync(@RequestParam(required = false) String message) {
        LOGGER.info("#### Begin handling error when full sync fails {}", message);

        Map<String, Object> configValues = new HashMap<>();
        configValues.put("sendDeltaToBothRedis", 0);
        configValues.put("processedQueueCount", 0);
        configValues.put("status", CONSTS.NO_SYNC_IN_PROGRESS);
        configValues.put("resumedQueue", new ArrayList<>());
        configValues.put("pausedQueue", gcpCloudTaskService.pauseQueueActivity(CONSTS.BACKLOG_QUEUE_PATH));
        cacheService.updateCacheValues(configValues, true);
        gcpCloudTaskService.purgeQueue(CONSTS.BACKLOG_QUEUE_PATH);
        String errorMessage = "Full sync process failed -" + message + ", please check logs \n, Queues have been purged";
        emailService.triggerEmail("FULL SYNC PROCESS FAILED", errorMessage, "ON_SYNC_ERROR");
    }

    /**
     * This controller is for receiving notification when the full sync file is successfully loaded into the Inactive redis instance
     * set status to FULL_SYNC_LOADED
     * send endTask to queues
     * Resume queue-group-1 activity
     */
    @GetMapping("/fileLoaded")
    public ResponseEntity<String> fullSyncFileLoaded() {
        LOGGER.info("#### Begin AllocateController fullSyncFileLoaded=> full sync file loaded successfully");
        if (CONSTS.config.getStatus() == CONSTS.FULL_SYNC_IN_PROGRESS) {
            Map<String, Object> configValues = new HashMap<>();
            gcpCloudTaskService.sendEndSyncTasks(CONSTS.PAUSED_QUEUE_PATHS);
            configValues.put("sendDeltaToBothRedis", 1);
            configValues.put("status", CONSTS.FULL_SYNC_LOADED);
            configValues.put("resumedQueue", gcpCloudTaskService.resumeQueueActivity(CONSTS.PAUSED_QUEUE_PATHS));
            cacheService.updateCacheValues(configValues, true);
            redisService.startTimerForSyncProcess();
            return new ResponseEntity<>("File load process completed successfully", HttpStatus.OK);
        } else {
            LOGGER.error("Error occurred after file is loaded to redis");
            handleErrorOnSync("occurred after file is loaded to redis \n File already loaded or Sync Process not started! ");
            return new ResponseEntity<>("File already loaded or Sync Process not started!", HttpStatus.BAD_REQUEST);
        }
    }


    /**
     * This controller is for receiving end sync message from backlog queue
     * Check for depth of queue from which the end sync is received
     * If depth is not zero repost end sync message to the same queue, else increase the counter to track the completion
     * start timer if all the Backlog queues are processed
     *
     * @param queuePath
     */
    @GetMapping("/handleEndSync")
    public void processEndSyncTask(@RequestParam String queuePath) {
        LOGGER.info("#### Begin AllocateController processEndSyncTask form queue={} ", () -> queuePath);
        try {
            Map<String, Object> configValues = new HashMap<>();
            long depth = gcpCloudTaskService.geQueueDepth(queuePath);
            if (depth == 0 || (depth <= appProp.getDepth() + 1 && depth != -1)) {
                LOGGER.debug("#### all delta messages are processed in  {}", () -> queuePath);
                int processedQueues = gcpCloudTaskService.getProcessedQueueCount(LocationName.of(appProp.getProjectId(), appProp.getLocation()).toString());
                if (processedQueues == -1) {
                    gcpCloudTaskService.sendEndSyncTask(queuePath);
                }
                configValues.put("processedQueueCount", processedQueues);
                if (processedQueues > CONSTS.config.getProcessedQueueCount()) {
                    LOGGER.debug("#### processed queue count={} ", () -> processedQueues);
                    CONSTS.config.setProcessedQueueCount(processedQueues);
                    cacheService.updateCacheValues(configValues, false);
                    if (CONSTS.config.getProcessedQueueCount() == CONSTS.PAUSED_QUEUE_PATHS.size()) {
                        LOGGER.debug("#### Marking full sync as complete");
                        configValues.put("status", CONSTS.FULL_SYNC_COMPLETED);
                        configValues.put("resumedQueue", new ArrayList<>());
                        configValues.put("processedQueueCount", 0);
                        LOGGER.debug("status:{} ", CONSTS.config.getStatus());
                        if (CONSTS.config.getStatus() == CONSTS.FULL_SYNC_LOADED) {
                            List<String> pausedQueues = gcpCloudTaskService.pauseQueueActivity(CONSTS.BACKLOG_QUEUE_PATH);
                            configValues.put("pausedQueue", pausedQueues);
                            redisService.switchRedisInstance(configValues, false);
                            redisService.startTimerBK();
                        }
                    }
                }

            } else {
                gcpCloudTaskService.sendEndSyncTask(queuePath);
            }
        } catch (Exception e) {
            LOGGER.error("Error while handling end sync message {}", e.getMessage());
            emailService.triggerEmail("Failure in end sync handler", "End sync messages processing failed", "END_SYNC_PROCESS_FAILED");
        }
    }


    /***
     * API used by support team
     */

    /**
     * API to rollback to previous redis instance based on conditional checks
     *
     * @return
     */
    @GetMapping("rollBack")
    public ResponseEntity<?> rollback() {
        try {
            redisService.rollBackSwitch();
            return new ResponseEntity<>("Instance rollbacked successfully", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    /**
     * API to force switch to Inactive redis instance
     *
     * @return
     */
    @GetMapping("forceSwitch")
    public ResponseEntity<?> forceSwitch() {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("sendDeltaToBothRedis", 0);
            map.put("status", CONSTS.NO_SYNC_IN_PROGRESS);
            redisService.switchRedisInstance(map, true);
            emailService.triggerEmail("Redis Instance force switched", "Redis Instances have been switched forcefully", "REDIS_FORCE_SWITCH");
            return new ResponseEntity<>("Instance switched successfully", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
