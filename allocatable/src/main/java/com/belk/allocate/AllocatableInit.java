package com.belk.allocate;

import com.belk.allocate.gcp.AppProp;
import com.belk.allocate.gcp.GCPStorageService;
import com.belk.allocate.hazel.CacheEventListener;
import com.belk.allocate.hazel.CacheService;
import com.belk.allocate.hazel.HandleConfigChange;
import com.hazelcast.core.HazelcastInstance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

import static com.belk.allocate.req.deltarequest.BasePojo._POJOGSON;

@Component
public class AllocatableInit {
    private static final Logger LOGGER = LogManager.getLogger(AllocatableInit.class);
    @Autowired
    HazelcastInstance hazelcastInstance;
    @Autowired
    private AppProp appProp;
    @Autowired
    private HandleConfigChange handleConfigChange;
    @Autowired
    private GCPStorageService gcpStorageService;
    @Autowired
    private CacheEventListener cacheEventListener;


    public Map<String, Object> readConfigValuesFromGCS() {
        try {
            Map<String, Object> configValues = _POJOGSON.fromJson(gcpStorageService.readFromStorage(appProp.getGcsFile()), Map.class);
            handleConfigChange.handleConfigChange(configValues);
            LOGGER.debug("Current config values {}", configValues);
            return configValues;
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Error while loading config values! =>{}", e.getMessage());
        }
        return new HashMap<>();
    }

    @PostConstruct
    public void init() {
        LOGGER.info("Initializing required values on startup");

        CacheService.map = hazelcastInstance.getMap(CacheService.CONFIG);
        CacheService.map.addEntryListener(cacheEventListener, true);
        if (!(CacheService.map.size() > 0)) {
            LOGGER.info("Config Values not present in cache, fetching values from GCS file....");
            Map<String, Object> map = readConfigValuesFromGCS();
            CacheService.map.putAll(map);
        } else {
            handleConfigChange.handleConfigChange(CacheService.map);
        }
        LOGGER.info("Values initialized");
    }


}
