package com.belk.allocate;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */

/**
 * @author AFUMYD1
 *
 */
public class SimpleCallStats {
    private String name = "Not Assigned";
    private long count = 0;
    private double totalTime = 0;
    private double lowWatermark = 0;
    private double highWatermark = 0;
    private double curTimeTaken = 0;
    private List<Double> mostRecentTimes = null;
    private double mostRecentTimesTotal = 0;
    private int maxMovingAvgCount = 10;

    /**
     * Constructor
     */
    public SimpleCallStats(String name) {
        this(name, 10);
    }

    /**
     * Constructor - name and moving avg count
     */
    public SimpleCallStats(String name, int maxMovingAvgCount) {
        if (name != null && name.trim().length() > 0) {
            this.name = name;
        }
        if (maxMovingAvgCount > 0) {
            this.maxMovingAvgCount = maxMovingAvgCount;
        }
        mostRecentTimes = new ArrayList<Double>(maxMovingAvgCount);
    }

    /**
     * Add call time
     *
     * @param timeTaken for the call
     */
    public void addCallTime(double timeTaken) {
        count++;
        totalTime += timeTaken;
        if (lowWatermark > timeTaken || lowWatermark == 0) {
            lowWatermark = timeTaken;
        }
        if (highWatermark < timeTaken) {
            highWatermark = timeTaken;
        }
        this.curTimeTaken = timeTaken;
        if (mostRecentTimes.size() == maxMovingAvgCount) {
            double d = mostRecentTimes.remove(0);
            mostRecentTimesTotal -= d;
        }
        mostRecentTimes.add(timeTaken);
        mostRecentTimesTotal += timeTaken;
    }

    /**
     * @return count
     */
    public long getCount() {
        return count;
    }

    /**
     * @return
     */
    public double getTotalTime() {
        return totalTime;
    }

    /**
     * @return
     */
    public double getLowWatermark() {
        return lowWatermark;
    }

    /**
     * @return
     */
    public double getHighWatermark() {
        return highWatermark;
    }

    /**
     * @return
     */
    public double getCurTimeTaken() {
        return curTimeTaken;
    }

    /**
     * Reset the stat block
     */
    public void resetStats() {
        count = 0;
        totalTime = 0;
        lowWatermark = 0;
        highWatermark = 0;
        mostRecentTimes.clear();
        mostRecentTimesTotal = 0;
    }

    /**
     * Get printable block
     *
     * @return String readable block of info
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("==> Stats block for " + name + " <==");
        sb.append(" #CTime: " + getCurTimeTaken());
        sb.append(" #TCount: " + getCount());
        if (count > 0) sb.append(" #Avg-Tim: " + getTotalTime() / getCount());
        if (count > 0)
            sb.append(" #MAvg-" + mostRecentTimes.size() + ": " + mostRecentTimesTotal / mostRecentTimes.size());
        sb.append(" #Max-Tim: " + getHighWatermark());
        sb.append(" #Min-Tim: " + getLowWatermark());
        return sb.toString();
    }
}
