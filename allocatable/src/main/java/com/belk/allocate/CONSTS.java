package com.belk.allocate;

import com.belk.allocate.hazel.Config;
import com.belk.allocate.req.deltarequest.LocationIgnore;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.*;

public class CONSTS {
    public static final int NO_SYNC_IN_PROGRESS = -1;
    public static final int FULL_SYNC_IN_PROGRESS = 1;
    public static final int FULL_SYNC_LOADED = 2;
    public static final int FULL_SYNC_COMPLETED = 3;

    public static Map<String, String> CONFIG_KEYS = new HashMap<>();
    public static Config config = new Config();
    public static int ACTIVE_INSTANCE = 1;
    public static int STATUS = -1;
    public static int SEND_DELTA_TO_BOTH_REDIS = 0;
    public static int PROCESSED_QUEUE = 0;
    public static List<String> BACKLOG_QUEUE_PATH = new ArrayList<>();
    public static List<String> PAUSED_QUEUE_PATHS = new ArrayList<>();
    public static List<String> RESUMED_QUEUE_PATH = new ArrayList<>();
    public static List<String> DELTA_QUEUE_PATH = new ArrayList<>();
    //This is used
    public static SimpleCallStats SERVICEMULTISTATSFOR_ = new SimpleCallStats("SMSF");
    public static SimpleCallStats DELTASTATSTASK_ = new SimpleCallStats("DLT");
    public static SimpleCallStats DELTATASKPROCESS_ = new SimpleCallStats("DTP");
    public static SimpleCallStats SERVICEMULTISTATS_ = new SimpleCallStats("SMS");
    public static SimpleCallStats CACHESTATS_ = new SimpleCallStats("CS");
    public static SimpleCallStats ARRAYLOOPSTATS_ = new SimpleCallStats("ALL");
    public static SimpleCallStats DELTASTATS_ = new SimpleCallStats("DLL");
    public static LocationIgnore LOCATIONIGNORE = new LocationIgnore();
    public static Set<String> LOCATIONIGNORESET = null;
    public static String ALLOCATABLEFIRSTKEY = "A:";
    public static String UPCLOCFIRSTKEY = "U:";
    public static RedisTemplate<String, Object> ACTIVE_REDIS_INSTANCE = new RedisTemplate<>();
    public static RedisTemplate<String, Object> IN_ACTIVE_REDIS_INSTANCE = new RedisTemplate<>();


}