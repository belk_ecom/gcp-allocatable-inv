package com.belk.allocate.logger;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class LoggerServiceUtil {


    private static final Map<LogLevel, Level> LOG_LEVELS;
    private static final Logger LOGGER = LogManager.getLogger(LoggerServiceUtil.class);

    static {
        Map<LogLevel, Level> levels = new HashMap<>();
        levels.put(LogLevel.TRACE, Level.TRACE);
        levels.put(LogLevel.DEBUG, Level.DEBUG);
        levels.put(LogLevel.INFO, Level.INFO);
        levels.put(LogLevel.WARN, Level.WARN);
        levels.put(LogLevel.ERROR, Level.ERROR);
        levels.put(LogLevel.FATAL, Level.ERROR);
        levels.put(LogLevel.OFF, Level.OFF);
        LOG_LEVELS = Collections.unmodifiableMap(levels);
    }

    public String setLogLevel(String logLevel, String packageName) {

        String methodName = "setLogLevel";

        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();


        LOGGER.info("Present Log level: " + loggerContext.getLogger(packageName).getLevel());
        loggerContext.getLogger(packageName).setLevel(LOG_LEVELS.get(LogLevel.valueOf(logLevel)));

        String currentLogLevel = loggerContext.getLogger(packageName).getLevel().toString();
        LOGGER.info("Log level set to : " + loggerContext.getLogger(packageName).getLevel());


        return currentLogLevel;
    }


    public LogLevel getCurrentLogLevel(String loggerName) {
        Level currentLevel = ((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger(loggerName).getLevel();
        for (Map.Entry<LogLevel, Level> entry : LOG_LEVELS.entrySet()) {
            if (entry.getValue() == currentLevel) {
                return entry.getKey();
            }
        }
        // If somehow unknown...
        return LogLevel.OFF;
    }

}
