package com.belk.allocate.logger;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.logging.LogLevel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class LoggerController {

    private static final Logger LOGGER = LogManager.getLogger(LoggerController.class);

    @Autowired
    LoggerServiceUtil serviceUtil;

    @PostMapping(value = "/logger/{package}/{loglevel}")
    public ResponseEntity<Object> loglevel(@PathVariable("loglevel") String logLevel, @PathVariable(value = "package") String packageName) {
        String currentLogLevel = serviceUtil.setLogLevel(logLevel, packageName);
        Map<String, String> level = new HashMap<>();
        level.put("package", packageName);
        level.put("logLevel", currentLogLevel);
        return new ResponseEntity<>(level, HttpStatus.OK);
    }

    @GetMapping(value = "/logger/{packageName}")
    public ResponseEntity<Object> getLogLevel(@PathVariable() String packageName) {
        String methodName = "getLogLevel";

        LogLevel currentLogLevel = serviceUtil.getCurrentLogLevel(packageName);

        return new ResponseEntity<>(currentLogLevel.name(), HttpStatus.OK);
    }
}
