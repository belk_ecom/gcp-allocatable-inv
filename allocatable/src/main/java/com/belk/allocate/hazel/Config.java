package com.belk.allocate.hazel;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
public class Config implements Cloneable {
    private int activeRedisInstance;
    private int status;
    private List<String> queueGroupOne = new ArrayList<>();
    private List<String> queueGroupTwo = new ArrayList<>();
    private Set<String> locationIgnore = new HashSet<>(0);
    private int processedQueueCount;
    private int sendDeltaToBothRedis;
    private List<String> pausedQueue = new ArrayList<>();
    private List<String> resumedQueue = new ArrayList<>();
    private List<String> alertTo = new ArrayList<>();
    private List<String> ipAddress = new ArrayList<>();
    private long maxDiffSize;
    private long minDiffSize;
    private long waitTime;
    private boolean sizeCheckRequired;
    private Map<String,String> loglevel;
    private long syncProcessLimit;
    private boolean syncCompleteNotification;
    private int maxTaskRetryCount;
    private String deltaUpdateSHA;
    private String safetyStockSHA;
    private String writeServiceIp;

}
