package com.belk.allocate.hazel;

import com.belk.allocate.CONSTS;
import com.belk.allocate.gcp.AppProp;
import com.belk.allocate.logger.LoggerServiceUtil;
import com.belk.allocate.redis.RedisTemplateInstance;
import com.belk.allocate.redis.RedisUserRepository;
import com.google.cloud.tasks.v2.QueueName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class HandleConfigChange {
    private static final Logger LOGGER = LogManager.getLogger(HandleConfigChange.class);
    @Autowired
    RedisUserRepository redisUserRepository;
    @Autowired
    RedisTemplateInstance redisTemplateInstance;
    @Autowired
    AppProp appProp;
    @Autowired
    LoggerServiceUtil serviceUtil;

    /**
     * This method handles cache value updates and set these values in local context
     *
     * @param configValues
     */
    public void handleConfigChange(Map<String, Object> configValues) {
        for (String key : configValues.keySet()) {
            switch (key) {
                case "activeRedisInstance":
                    LOGGER.debug("Value of active redis is changed from {} to {}", CONSTS.ACTIVE_INSTANCE, configValues.get(key));
                    CONSTS.ACTIVE_INSTANCE = (int) Double.parseDouble(configValues.get(key).toString());
                    redisUserRepository.setRedisTemplate(redisTemplateInstance.getRedisTemplate(CONSTS.ACTIVE_INSTANCE));
                    CONSTS.ACTIVE_REDIS_INSTANCE = redisTemplateInstance.getActiveRedisTemplate();
                    CONSTS.IN_ACTIVE_REDIS_INSTANCE = redisTemplateInstance.getInActiveRedisTemplate();
                    CONSTS.config.setActiveRedisInstance(CONSTS.ACTIVE_INSTANCE);
                    break;
                case "status":
                    LOGGER.debug("Value of STATUS is changed from {} to {}", CONSTS.STATUS, configValues.get(key));
                    CONSTS.STATUS = (int) Double.parseDouble(configValues.get(key).toString());
                    CONSTS.config.setStatus(CONSTS.STATUS);
                    break;
                case "queueGroupOne":
                    LOGGER.debug("Value of queueGroupOne redis is changed from {} to {}", CONSTS.BACKLOG_QUEUE_PATH, configValues.get(key));
                    ArrayList<String> queues = (ArrayList<String>) configValues.get(key);
                    CONSTS.BACKLOG_QUEUE_PATH = createQueuePath(queues);
                    CONSTS.config.setQueueGroupOne(queues);

                    break;
                case "queueGroupTwo":
                    LOGGER.debug("Value of queueGroupTwo redis is changed from {} to {}", CONSTS.DELTA_QUEUE_PATH, configValues.get(key));
                    ArrayList<String> queues1 = (ArrayList<String>) configValues.get(key);
                    CONSTS.DELTA_QUEUE_PATH = createQueuePath(queues1);
                    CONSTS.config.setQueueGroupTwo(queues1);

                    break;
                case "locationIgnore":
                    LOGGER.debug("Location values updated!");

                    if (configValues.get(key) != null) {
                        if (configValues.get(key) instanceof ArrayList) {
                            ArrayList<String> locationIgnore = (ArrayList<String>) configValues.get(key);
                            if (locationIgnore != null && locationIgnore.size() > 0) {
                                CONSTS.LOCATIONIGNORE.setLoc_id(new HashSet<>(locationIgnore));
                                CONSTS.config.setLocationIgnore(CONSTS.LOCATIONIGNORE.getLoc_id());
                            }
                        } else {
                            HashSet<String> locationIgnore = (HashSet<String>) configValues.get(key);
                            if (locationIgnore != null && locationIgnore.size() > 0) {
                                CONSTS.LOCATIONIGNORE.setLoc_id(locationIgnore);

                                CONSTS.config.setLocationIgnore(CONSTS.LOCATIONIGNORE.getLoc_id());
                            }

                        }

                    }
                    break;
                case "processedQueueCount":
                    LOGGER.debug("Value of processedQueueCount is changed from {} to {}", CONSTS.PROCESSED_QUEUE, configValues.get(key));
                    CONSTS.PROCESSED_QUEUE = (int) Double.parseDouble(configValues.get(key).toString());
                    CONSTS.config.setProcessedQueueCount(CONSTS.PROCESSED_QUEUE);
                    break;
                case "sendDeltaToBothRedis":
                    LOGGER.debug("Value of sendDeltaToBothRedis is changed from {} to {}", CONSTS.SEND_DELTA_TO_BOTH_REDIS, configValues.get(key));
                    CONSTS.SEND_DELTA_TO_BOTH_REDIS = (int) Double.parseDouble(configValues.get(key).toString());
                    CONSTS.config.setSendDeltaToBothRedis(CONSTS.SEND_DELTA_TO_BOTH_REDIS);
                    break;
                case "pausedQueue":
                    LOGGER.debug("Value of pausedQueues  is changed from {} to {}", CONSTS.PAUSED_QUEUE_PATHS, configValues.get(key));
                    CONSTS.PAUSED_QUEUE_PATHS = (List<String>) configValues.get(key);
                    CONSTS.config.setPausedQueue(CONSTS.PAUSED_QUEUE_PATHS);
                    break;
                case "resumedQueue": {
                    LOGGER.debug("Value of resumedQueues  is changed from {} to {}", CONSTS.RESUMED_QUEUE_PATH, configValues.get(key));
                    CONSTS.RESUMED_QUEUE_PATH = (List<String>) configValues.get(key);
                    CONSTS.config.setResumedQueue(CONSTS.RESUMED_QUEUE_PATH);
                    break;
                }
                case "waitTime":
                    LOGGER.debug("Value of waitTime  is changed from {} to {}", CONSTS.config.getWaitTime(), configValues.get(key));
                    CONSTS.config.setWaitTime((long) Double.parseDouble(configValues.get("waitTime").toString()));
                    break;

                case "maxDiffSize": {
                    LOGGER.debug("Value of max diffSize  is changed from {} to {}", CONSTS.config.getMaxDiffSize(), configValues.get(key));
                    CONSTS.config.setMaxDiffSize((long) Double.parseDouble(configValues.get("maxDiffSize").toString()));

                    break;
                }
                case "minDiffSize": {
                    LOGGER.debug("Value of min diffSize  is changed from {} to {}", CONSTS.config.getMinDiffSize(), configValues.get(key));
                    CONSTS.config.setMinDiffSize((long) Double.parseDouble(configValues.get("minDiffSize").toString()));

                    break;
                }
                case "sizeCheckRequired": {
                    LOGGER.debug("Value of sizeCheckRequired  is changed from {} to {}", CONSTS.config.isSizeCheckRequired(), configValues.get(key));
                    CONSTS.config.setSizeCheckRequired(Boolean.parseBoolean(configValues.get("sizeCheckRequired").toString()));
                    break;
                }
                case "alertTo": {
                    LOGGER.debug("Value of toEmail  is changed from {} to {}", CONSTS.config.getAlertTo(), configValues.get(key));
                    List<String> toEmails = (List<String>) configValues.get(key);
                    CONSTS.config.setAlertTo(toEmails);

                    break;
                }
                case "loglevel": {
                    LOGGER.debug("Log level  is changed");
                    Map<String, String> map = (Map<String, String>) configValues.get(key);
                    if (!(map.containsKey("logLevel") && map.containsKey("package"))) {
                        LOGGER.error("Invalid log request");
                        break;
                    }
                    if (map.containsKey("workloadType") && map.get("workloadType") != null)
                        if (Arrays.asList(map.get("workloadType").split(",")).indexOf(System.getenv("workloadType")) != -1) {
                            serviceUtil.setLogLevel(map.get("logLevel"), map.get("package"));
                        } else {
                            serviceUtil.setLogLevel(map.get("logLevel"), map.get("package"));
                        }
                    CONSTS.config.setLoglevel(map);

                    break;
                }
                case "syncProcessLimit": {
                    LOGGER.debug("sync process limit changed from {} to {}", CONSTS.config.getSyncProcessLimit(), configValues.get(key));
                    CONSTS.config.setSyncProcessLimit((long) Double.parseDouble(configValues.get("syncProcessLimit").toString()));
                    break;
                }
                case "syncCompleteNotification": {
                    LOGGER.debug("Sync complete notification changed from {} to {}", CONSTS.config.isSyncCompleteNotification(), configValues.get(key));
                    CONSTS.config.setSyncCompleteNotification(Boolean.parseBoolean(configValues.get("syncCompleteNotification").toString()));
                    break;
                }
                case "maxTaskRetryCount": {
                    LOGGER.debug("maxTaskRetryCount changed from {} to {}", CONSTS.config.getMaxTaskRetryCount(), configValues.get(key));
                    CONSTS.config.setMaxTaskRetryCount((int) Double.parseDouble(configValues.get(key).toString()));
                    break;

                }
                case "deltaUpdateSHA":{
                    LOGGER.debug("compareAndLoadSHA changed from {} to {}", CONSTS.config.getDeltaUpdateSHA(),configValues.get(key));
                    CONSTS.config.setDeltaUpdateSHA(configValues.get(key).toString());
                    break;
                }
                case "safetyStockSHA":{
                    LOGGER.debug("shiftBucketsSHA changed from {} to {}", CONSTS.config.getSafetyStockSHA(),configValues.get(key));
                    CONSTS.config.setSafetyStockSHA(configValues.get(key).toString());
                    break;
                }
                case "writeServiceIp":{
                    LOGGER.debug("writeServiceIp changed from {} to {}", CONSTS.config.getWriteServiceIp(),configValues.get(key));
                    CONSTS.config.setWriteServiceIp(configValues.get(key).toString());
                    break;
                }
                default:
                    LOGGER.error("Invalid Key in the config {}", key);

            }
        }

    }

    /**
     * Creates the Cloud Task and the queue at the class level so that it doesn't have to be created for every request.
     */
    public List<String> createQueuePath(List<String> queues) {
        List<String> queuePath = new ArrayList<>();
        try {

            for (String queue : queues) {
                queuePath.add(QueueName.of(appProp.getProjectId(), appProp.getLocation(), queue).toString());
            }

        } catch (Exception e) {

            LOGGER.error("Exception while generating queue Path {}", e.getMessage());
        }
        return queuePath;
    }

}
