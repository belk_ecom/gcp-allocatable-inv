package com.belk.allocate.hazel;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.map.listener.EntryAddedListener;
import com.hazelcast.map.listener.EntryUpdatedListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CacheEventListener implements
        EntryAddedListener<String, Object>,
        EntryUpdatedListener<String, Object> {

    @Autowired
    HandleConfigChange handleConfigChange;

    /**
     * This event is triggered when a new cache entry is added
     *
     * @param event
     */
    @Override
    public void entryAdded(EntryEvent<String, Object> event) {
        if (!event.getKey().equals("ipAddress")) {
            Map<String, Object> map = new HashMap<>();
            map.put(event.getKey(), event.getValue());
            handleConfigChange.handleConfigChange(map);
        }
    }

    /**
     * This event is triggered when a cache value is updated
     *
     * @param event
     */
    @Override
    public void entryUpdated(EntryEvent<String, Object> event) {
        if (!event.getKey().equals("ipAddress")) {
            Map<String, Object> map = new HashMap<>();
            map.put(event.getKey(), event.getValue());
            handleConfigChange.handleConfigChange(map);
        }
    }


}