package com.belk.allocate.hazel;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
public class CacheConfig {
    @Autowired
    CacheMemberListener cacheMemberListener;

    @Bean("hazelcastInstance")
    @ConditionalOnProperty(name = "spring.profiles.active",havingValue = "local")
    public HazelcastInstance hazelcastInstance() {
        HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();
        hazelcastInstance.getCluster().addMembershipListener(cacheMemberListener );
        hazelcastInstance.getLocalEndpoint();
        return hazelcastInstance;
    }


    @Bean("hazelcastInstance")
    @ConditionalOnProperty(name = "spring.profiles.active",havingValue = "dev")
    public HazelcastInstance hazelcastInstanceDev() {
        com.hazelcast.config.Config config = new com.hazelcast.config.Config();
        config.getNetworkConfig().getJoin().getTcpIpConfig().setEnabled(false);
        config.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(false);
        config.getNetworkConfig().getJoin().getKubernetesConfig().setEnabled(true);
        HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance(config);
        hazelcastInstance.getCluster().addMembershipListener(cacheMemberListener);
        return hazelcastInstance;
    }

    @Bean("hazelcastInstance")
    @ConditionalOnProperty(name = "spring.profiles.active",havingValue = "prod")
    public HazelcastInstance hazelcastInstanceProd() {

        com.hazelcast.config.Config config = new com.hazelcast.config.Config();
        config.getNetworkConfig().getJoin().getTcpIpConfig().setEnabled(false);
        config.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(false);
        config.getNetworkConfig().getJoin().getKubernetesConfig().setEnabled(true);
        HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance(config);
        hazelcastInstance.getCluster().addMembershipListener(cacheMemberListener);
        return hazelcastInstance;
    }

}

