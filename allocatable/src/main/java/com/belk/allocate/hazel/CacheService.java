package com.belk.allocate.hazel;

import com.belk.allocate.CONSTS;
import com.belk.allocate.gcp.AppProp;
import com.belk.allocate.gcp.GCPStorageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.map.IMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static com.belk.allocate.req.deltarequest.BasePojo._POJOGSON;

@Service
public class CacheService {

    public static final String CONFIG = "config";
    public static IMap<String, Object> map;

    @Autowired
    HandleConfigChange handleConfigChange;
    @Autowired
    GCPStorageService gcpStorageService;
    @Autowired
    AppProp appProp;

    /**
     * Add/Update value to hazel cast cache
     *
     * @param number
     * @param value
     * @return
     */
    public Object put(String number, Object value) {
        return map.put(number, value);
    }

    /**
     * Get value from cache by key
     *
     * @param key
     * @return
     */
    public Object get(String key) {
        return map.get(key);
    }


    /**
     * Update cache values
     *
     * @param cacheValues
     */
    public void updateCacheValues(Map<String, Object> cacheValues, Boolean commit) {
        if (commit) {
            handleConfigChange.handleConfigChange(cacheValues);
            ObjectMapper oMapper = new ObjectMapper();
            Map<String, Object> map1 = new HashMap<>();
            map1 = oMapper.convertValue(CONSTS.config, Map.class);
            map1.remove("locationIgnore");
            gcpStorageService.writeToStorage(appProp.getGcsFile(), _POJOGSON.toJson(map1));
        }
        map.putAll(cacheValues);
    }


}
