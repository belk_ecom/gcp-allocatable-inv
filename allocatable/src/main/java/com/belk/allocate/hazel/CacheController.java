package com.belk.allocate.hazel;

import com.belk.allocate.gcp.AppProp;
import com.belk.allocate.gcp.GCPStorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import static com.belk.allocate.req.deltarequest.BasePojo._POJOGSON;


@RestController
public class CacheController {
    private static final Logger LOGGER = LogManager.getLogger(CacheController.class);
    @Autowired
    CacheService cacheService;
    @Autowired
    AppProp appProp;
    @Autowired
    GCPStorageService gcpStorageService;

    /**
     * Get cache value
     *
     * @param keys
     * @return all cache values or returns cache values for particular keys
     * @throws IOException
     */
    @GetMapping("config/get")
    public Object getConfigValues(@RequestParam(required = false) Set<String> keys) throws IOException {
        if (keys != null && keys.size() > 0) {
            return CacheService.map.getAll(keys);
        }
        return CacheService.map;
    }

    /**
     * Refresh cache
     */
    @GetMapping("config/refresh")
    public void refreshCache() throws IOException {
        Map<String, Object> configValues = _POJOGSON.fromJson(gcpStorageService.readFromStorage(appProp.getGcsFile()), Map.class);
        cacheService.updateCacheValues(configValues, false);
    }

    /**
     * Update cache values
     *
     * @param config
     */
    @PostMapping("config/update")
    public void write(@RequestBody Map<String, Object> config) {
        cacheService.updateCacheValues(config, true);
    }


}
