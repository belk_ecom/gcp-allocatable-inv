package com.belk.allocate.hazel;

import com.belk.allocate.CONSTS;
import com.belk.allocate.gcp.AppProp;
import com.belk.allocate.gcp.GCPStorageService;
import com.hazelcast.cluster.Member;
import com.hazelcast.cluster.MembershipEvent;
import com.hazelcast.cluster.MembershipListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.belk.allocate.req.deltarequest.BasePojo._POJOGSON;

@Component
public class CacheMemberListener implements MembershipListener {
    @Autowired
    CacheService cacheService;
    @Autowired
    AppProp appProp;
    @Autowired
    GCPStorageService gcpStorageService;

    /**
     * This event is triggered when a new member is added to cluster
     *
     * @param membershipEvent
     */
    public void memberAdded(MembershipEvent membershipEvent) {
        List<String> members = new ArrayList<>();
        for (Member member : membershipEvent.getMembers()) {
            members.add(member.getSocketAddress().getHostString());
        }
        CacheService.map.put("ipAddress", members);
        CONSTS.config.setIpAddress(members);
    }

    /**
     * This event is triggered when a member is removed from the cache cluster
     *
     * @param membershipEvent
     */
    public void memberRemoved(MembershipEvent membershipEvent) {
        List<String> members = new ArrayList<>();
        for (Member member : membershipEvent.getMembers()) {
            members.add(member.getSocketAddress().getHostString());
        }
        CONSTS.config.setIpAddress(members);
        try {
            Map<String, Object> configValues = new HashMap<>();
            configValues = _POJOGSON.fromJson(gcpStorageService.readFromStorage(appProp.getGcsFile()), Map.class);
            configValues.put("ipAddress", members);
            cacheService.updateCacheValues(configValues, false);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
